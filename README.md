# About
 * Source codes for Anime Maker

# Description
 * `anime_maker_rgb_5x5x5_led_cube_lib` - non-standard libraries and settings
 * `Debug` - temporary folder when compile
 * `generated_animations` - when user type "exit" animation will be saved to
      this folder
 * `anime_maker_rgb_5x5x5_led_cube.c` - main program
 * `makefile` - makefile for this project :)
 * `anime_maker_rgb_5x5x5_led_cube` - Linux 64bit binary for Core2 Duo and higher

###### Martin Stejskal - martin.stej [at] gmail.com