/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief When user type command, some functions must process it
 *
 * This is set of functions, that test input command and if match, then do\n
 * their data process
 */


#ifndef _test_for_library_
#define _test_for_library_

#include <inttypes.h>   // Data types
#include <string.h>     // Library for string operations
#include <stdio.h>      // Standard I/O

#include <unistd.h>     // if needed sleep function

#include "animation_codes.h"
#include "settings.h"
#include "simple_functions.h"
#include "ask_for.h"

/* Following defines should not be changed until you really know what are you
 * doing!
 */
#define no_cancel_or_done_command       0       // No cancel or done
#define cancel_command                  1
#define done_command                    2


void test_for_EXIT(char *p_cmd, int *p_animation_done);

void test_for_HELP(char *p_cmd);

void test_for_ZEROS(char *p_cmd,
                    uint8_t  (*p_fb_data_virtual)[5][5][5],
                    uint8_t i_wall);

void test_for_ONES(char *p_cmd,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint8_t i_wall);

void test_for_PWM_RED(char *p_cmd,
                      uint8_t *p_pwm_r,
                      uint16_t (*p_commands)[max_commands_per_frame],
                      uint16_t *p_command_index);

void test_for_PWM_GREEN(char *p_cmd,
                      uint8_t *p_pwm_g,
                      uint16_t (*p_commands)[max_commands_per_frame],
                      uint16_t *p_command_index);

void test_for_PWM_BLUE(char *p_cmd,
                      uint8_t *p_pwm_b,
                      uint16_t (*p_commands)[max_commands_per_frame],
                      uint16_t *p_command_index);

void test_for_RANDOM_ALL_PWM(char    *p_cmd,
                             uint16_t (*p_commands)[max_commands_per_frame],
                             uint16_t *p_command_index,
                             uint32_t *p_frame,
                             uint8_t  (*p_fb_data_virtual)[5][5][5],
                             uint16_t (*p_anim_stream)[max_size_anim_stream],
                             int      *p_slowdown_factor,
                             uint8_t  *p_wall,
                             float    *p_virtual_time);

void test_for_LINE(char *p_cmd,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint8_t i_wall,
                   uint8_t i_led_level,
                   uint8_t i_column);

void test_for_COLUMN(char *p_cmd,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint8_t i_wall,
                   uint8_t i_led_level,
                   uint8_t i_column);

void test_for_ARROW_UP(char (*p_cmd)[max_command_length],
                       uint8_t *p_led_level);

void test_for_ARROW_DOWN(char (*p_cmd)[max_command_length],
                         uint8_t *p_led_level);

void test_for_ARROW_RIGHT(char (*p_cmd)[max_command_length],
                         uint8_t *p_column);

void test_for_ARROW_LEFT(char (*p_cmd)[max_command_length],
                         uint8_t *p_column);

void test_for_NEXT(char     *p_cmd,
                   uint8_t  *p_column,
                   uint8_t  *p_led_level,
                   uint8_t  *p_wall,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor,
                   uint8_t  i_do_not_write_data);

void test_for_BACK(char    *p_cmd,
                   uint8_t *p_column,
                   uint8_t *p_led_level,
                   uint8_t *p_wall);

void test_for_DATA(char   (*p_cmd)[max_command_length],
                   uint8_t  *p_column,
                   uint8_t  *p_led_level,
                   uint8_t  *p_wall,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor,
                   uint8_t  i_do_not_write_data);

void test_for_FONT(char *p_cmd,
                   uint8_t  *p_wall,
                   uint8_t  *p_pwm_r,
                   uint8_t  *p_pwm_g,
                   uint8_t  *p_pwm_b,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor);

void test_for_ROTATE(char *p_cmd,
                   uint8_t  *p_wall,
                   uint8_t  *p_pwm_r,
                   uint8_t  *p_pwm_g,
                   uint8_t  *p_pwm_b,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor);




#endif
