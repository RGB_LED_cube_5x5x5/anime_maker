/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Some functions are really simple, but they obstruct in main file
 */

#include "simple_functions.h"


void show_preview(uint8_t  (*p_fb_data_virtual)[5][5][5],
                  uint16_t i_command_index,
                  uint32_t i_frame_counter,
                  uint8_t  i_wall,
                  uint8_t  i_column,
                  uint8_t  i_led_level,
                  uint8_t  i_pwm_r,
                  uint8_t  i_pwm_g,
                  uint8_t  i_pwm_b,
                  float f_virtual_time)
{

  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| Frame: %9d | Wall: %d/4 | Column: %d/4 | Time: %7.2f s              |\n"
"+--------------+---+-----------++------------+--+---------------------------+"
"\n"
"| PWM red: %3d | PWM green: %3d | PWM blue: %3d | Commands on frame: %4d   |"
"\n"
"+--------------+----------------+---------------+---------------------------+"
"\n"
"|              | C0  |     | C1  |     | C2  |     | C3  |     | C4  |      |"
"\n"
"|                                                                           |"
"\n"
  , i_frame_counter, i_wall, i_column, f_virtual_time,
  i_pwm_r, i_pwm_g, i_pwm_b, i_command_index
  );

  for(int i=4 ; i>=0 ; i--)      // led level (LED 0 ~ LED 4)
  {
    printf("|    LED %d >   ", i);

    for(int j=0 ; j<5 ; j++)    // Collumn
    {
      // If actual "window" is active
      if( (i==i_led_level) && (j==i_column))
      {
        printf("| ");
      }
      else
      { // Else actual "window" is not active
        printf("  ");
      }

      // Test if red bit is set
      if( (*p_fb_data_virtual)[i_wall][i][j] & (1<<red_shift) )
      {
        printf("R");
      }
      else
      {
        printf("-");
      }
      // Test if green bit is set
      if( (*p_fb_data_virtual)[i_wall][i][j] & (1<<green_shift) )
      {
        printf("G");
      }
      else
      {
        printf("-");
      }
      // Test if blue bit is set
      if( (*p_fb_data_virtual)[i_wall][i][j] & (1<<blue_shift) )
      {
        printf("B");
      }
      else
      {
        printf("-");
      }

      // If actual "window" is active
      if( (i==i_led_level) && (j==i_column))
      {
        printf(" |     ");
      }
      else
      {
        printf("       ");
      }
    }
    //End of line
    printf(" |\n"
"|                                                                           |"
"\n");
  }

  // End of frame
  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| Type command or data. When type \"HELP\", you get list of commands"
"          |"
"\n"
"+---------------------------------------------------------------------------+"
"\n"
"| Howto type data (examples):                                               |"
"\n"
"| rgb -> turn on all LEDs in segment                                        |"
"\n"
"| r-- -> just turn on red LED                                               |"
"\n"
"| 011 -> turn green and blue LED                                            |"
"\n"
"| A   -> turn on all LEDs in segment                                        |"
"\n"
"| g   -> turn on green LED                                                  |"
"\n"




"+---------------------------------------------------------------------------+"
"\n"
"| Command line: "
  );
}



/*---------------------------------------------------------------------------*/



void write_frame(uint8_t  (*p_fb_data_virtual)[5][5][5],
                 uint16_t (*p_anim_stream)[max_size_anim_stream],
                 uint16_t (*p_commands),
                 uint32_t i_frame,
                 uint8_t  i_do_not_write_data)
{
  // Counts columns. If reached 25 (overflow) -> one frame
  uint8_t  i_column_cnt = 0;

  // Counts found fames
  uint32_t i_actual_frame = 0;

  // Counter for stream index
  uint64_t i_index_cnt = 0;

  // Loaded word from stream
  uint16_t i_anime_word;

  // First we need to find where to continue write data
  while( (i_actual_frame < i_frame) & (i_frame != 0) )
  {
    // Load first anime_word
    i_anime_word = (*p_anim_stream)[i_index_cnt];

    // Increase counter (next time load following word)
    i_index_cnt++;

    // Test if i_anime_word is data (increase i_column_cnt) or command
    if( (i_anime_word & 0x8000)==0 )
    {// If data -> increase i_column_cnt
      i_column_cnt++;

      // Ok, test if i_column_cnt is not 25 -> overflow -> next frame
      if(i_column_cnt == 25)
      {
        // If yes, then restart i_column_cnt
        i_column_cnt = 0;

        // And increase i_actual_frame
        i_actual_frame++;
      }

    }
    // Else command - test for cmd_end_of_3D_frame -> same as 25x data
    // Use masking -> get just command, not a value
    if( (i_anime_word & 0xFF00) == cmd_end_of_3D_frame )
    {
      i_actual_frame++;
    }

    // Test for cmd_rand_all_pwm
    if( (i_anime_word & 0xFF00) == cmd_rand_all_pwm )
    {// If value is not zero -> frame = frame + value;
      i_actual_frame = i_actual_frame + (i_anime_word & 0x00FF) +1;
    }
  }

  // OK, if we are out of while, then we add data from framebuffer
  i_anime_word = (*p_commands);      // Load first command


  // First write commands - write until there are commands
  while( (i_anime_word & 0x8000) != 0 )
  {
    (*p_anim_stream)[i_index_cnt] = i_anime_word;
    i_index_cnt++;

    // Increase pointer;
    p_commands++;
    // And load next command
    i_anime_word = (*p_commands);
  }


  if( i_do_not_write_data != 0 )
  {// If we do not want write data -> add command end of 3D frame
    (*p_anim_stream)[i_index_cnt] = cmd_end_of_3D_frame;
    return;     // And get out of here
  }


  // Now data
  for(int i=0 ; i<5 ; i++)      // Walls
  {
    for(int j=0 ; j<5 ; j++)    // Columns
    {
      // Clear variable
      i_anime_word = 0;

      for(int k=0 ; k<5 ; k++)  // LED level
      {
        // Red Color
        i_anime_word |=                                 // Add data
            (((*p_fb_data_virtual)[i][k][j]) & (0b100)) // Masking
              <<((red_shift*5)+k-red_shift);            // And bit shift

        // Red Green
        i_anime_word |=                                 // Add data
            (((*p_fb_data_virtual)[i][k][j]) & (0b010)) // Masking
              <<((green_shift*5)+k-green_shift);        // And bit shift

        // Red Blue
        i_anime_word |=                                 // Add data
            (((*p_fb_data_virtual)[i][k][j]) & (0b001)) // Masking
              <<((blue_shift*5)+k-blue_shift);          // And bit shift
      }

      // Write 16 bits
      (*p_anim_stream)[i_index_cnt++] = i_anime_word;
    }
  }
}



/*---------------------------------------------------------------------------*/



void load_character_to_framebuffer(char c_character,
                                   uint8_t  (*p_fb_data_virtual)[5][5][5])
{
  // Pointer to database
  const uint16_t *p_database;

  // First clear virtual framebuffer
  clear_virtual_framebuffer( p_fb_data_virtual );

  // Then search in database
  switch(c_character)
  {
  case 'A': p_database = &font_5x5_A[0]; break;
  case 'B': p_database = &font_5x5_B[0]; break;
  case 'C': p_database = &font_5x5_C[0]; break;
  case 'D': p_database = &font_5x5_D[0]; break;
  case 'E': p_database = &font_5x5_E[0]; break;
  case 'F': p_database = &font_5x5_F[0]; break;
  case 'G': p_database = &font_5x5_G[0]; break;
  case 'H': p_database = &font_5x5_H[0]; break;
  case 'I': p_database = &font_5x5_I[0]; break;
  case 'J': p_database = &font_5x5_J[0]; break;
  case 'K': p_database = &font_5x5_K[0]; break;
  case 'L': p_database = &font_5x5_L[0]; break;
  case 'M': p_database = &font_5x5_M[0]; break;
  case 'N': p_database = &font_5x5_N[0]; break;
  case 'O': p_database = &font_5x5_O[0]; break;
  case 'P': p_database = &font_5x5_P[0]; break;
  case 'Q': p_database = &font_5x5_Q[0]; break;
  case 'R': p_database = &font_5x5_R[0]; break;
  case 'S': p_database = &font_5x5_S[0]; break;
  case 'T': p_database = &font_5x5_T[0]; break;
  case 'U': p_database = &font_5x5_U[0]; break;
  case 'V': p_database = &font_5x5_V[0]; break;
  case 'W': p_database = &font_5x5_W[0]; break;
  case 'X': p_database = &font_5x5_X[0]; break;
  case 'Y': p_database = &font_5x5_Y[0]; break;
  case 'Z': p_database = &font_5x5_Z[0]; break;

  case '0': p_database = &font_5x5_0[0]; break;
  case '1': p_database = &font_5x5_1[0]; break;
  case '2': p_database = &font_5x5_2[0]; break;
  case '3': p_database = &font_5x5_3[0]; break;
  case '4': p_database = &font_5x5_4[0]; break;
  case '5': p_database = &font_5x5_5[0]; break;
  case '6': p_database = &font_5x5_6[0]; break;
  case '7': p_database = &font_5x5_7[0]; break;
  case '8': p_database = &font_5x5_8[0]; break;
  case '9': p_database = &font_5x5_9[0]; break;

  case 'a': p_database = &font_5x5_a[0]; break;
  case 'b': p_database = &font_5x5_b[0]; break;
  case 'c': p_database = &font_5x5_c[0]; break;
  case 'd': p_database = &font_5x5_d[0]; break;
  case 'e': p_database = &font_5x5_e[0]; break;
  case 'f': p_database = &font_5x5_f[0]; break;
  case 'g': p_database = &font_5x5_g[0]; break;
  case 'h': p_database = &font_5x5_h[0]; break;
  case 'i': p_database = &font_5x5_i[0]; break;
  case 'j': p_database = &font_5x5_j[0]; break;
  case 'k': p_database = &font_5x5_k[0]; break;
  case 'l': p_database = &font_5x5_l[0]; break;
  case 'm': p_database = &font_5x5_m[0]; break;
  case 'n': p_database = &font_5x5_n[0]; break;
  case 'o': p_database = &font_5x5_o[0]; break;
  case 'p': p_database = &font_5x5_p[0]; break;
  case 'q': p_database = &font_5x5_q[0]; break;
  case 'r': p_database = &font_5x5_r[0]; break;
  case 's': p_database = &font_5x5_s[0]; break;
  case 't': p_database = &font_5x5_t[0]; break;
  case 'u': p_database = &font_5x5_u[0]; break;
  case 'v': p_database = &font_5x5_v[0]; break;
  case 'w': p_database = &font_5x5_w[0]; break;
  case 'x': p_database = &font_5x5_x[0]; break;
  case 'y': p_database = &font_5x5_y[0]; break;
  case 'z': p_database = &font_5x5_z[0]; break;
  }

  for(uint8_t i=0 ; i<5; i++)   // Columns
  {
    for(uint8_t j=0 ; j<5 ; j++)        // led_level
    {
      // Add RED color (bit)
      (*p_fb_data_virtual)[0][j][i] |=
          (((*p_database)& (1<<(red_shift*5 + j)))>>(red_shift*5 + j))
              <<red_shift;
      // Add GREEN color (bit)
      (*p_fb_data_virtual)[0][j][i] |=
          (((*p_database)& (1<<(green_shift*5 + j)))>>(green_shift*5 + j))
              <<green_shift;
      // Add RED color (bit)
      (*p_fb_data_virtual)[0][j][i] |=
          (((*p_database)& (1<<(blue_shift*5 + j)))>>(blue_shift*5 + j))
              <<blue_shift;
    }
    // And move to next column
    p_database++;
  }
}



/*---------------------------------------------------------------------------*/



void clear_loaded_command(char *p_cmd)
{
  for(int i=0 ; i<max_command_length ; i++)
  {
    *p_cmd = '\0';    // Load NULL
    p_cmd++;          // Move to next
  }
}



/*---------------------------------------------------------------------------*/



void clear_virtual_framebuffer(uint8_t (*p_fb_data_vitual)[5][5][5])
{
  for(int i=0 ; i<5 ; i++)
  {
    for(int j=0 ; j<5 ; j++)
    {
      for(int k=0 ; k<5 ; k++)
      {
        (*p_fb_data_vitual)[i][j][k] = 0;
      }
    }
  }
}



/*---------------------------------------------------------------------------*/




void set_virtual_framebuffer(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  for(int i=0 ; i<5 ; i++)
  {
    for(int j=0 ; j<5 ; j++)
    {
      for(int k=0 ; k<5 ; k++)
      {
        (*p_fb_data_virtual)[i][j][k] = (1<<red_shift)|
                                        (1<<green_shift)|
                                        (1<<blue_shift);
      }

    }
  }
}




/*---------------------------------------------------------------------------*/



void clear_real_framebuffer(uint16_t (*p_anim_stream)[max_size_anim_stream])
{
  for(int i=0 ; i<max_size_anim_stream ; i++)
  {
    (*p_anim_stream)[i] = 0;
  }
}



/*---------------------------------------------------------------------------*/



void jump_out_to_next_screen(void)
{
  for(int i=0 ; i<screen_lines_max ; i++ )
  {
    printf("\n\n\n\n\n\n\n\n\n\n");
  }
}



/*---------------------------------------------------------------------------*/



void wait_for_user(void)
{
  getchar();    // Before flush must be read, else not work as expected
  fflush(stdin);// Clean input buffer

  // Variable for checking if user is ready
  char c_pressed_key = '\0';

  // Wait until any key pressed
  while( c_pressed_key == '\0')
  {
    c_pressed_key = getchar();
  }
}



/*---------------------------------------------------------------------------*/



void show_help_font_mode(void)
{
  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| cancel - aboard all commands done in this mode                            |"
"\n"
"| rotate (r) - rotation of 3D frame                                         |"
"\n"
"| next (n) - move to next wall                                              |"
"\n"
"| back (m) - move to previous wall                                          |"
"\n"
"| done (d) - when 3D frame is done                                          |"
"\n"
"+---------------------------------------------------------------------------+"
"\n"
  );
}



/*---------------------------------------------------------------------------*/



void show_help_rotate_mode(void)
{
  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| cancel - aboard all commands done in this mode                            |"
"\n"
"| fwd (f) - rotate forward (direction to user - from wall 4 to wall 3)      |"
"\n"
"| bwd (b) - rotate backward (direction from user - from wall 0 to wall 1)   |"
"\n"
"| left (l) - rotate left (from column 4 to column 3)                        |"
"\n"
"| right (r) - rotate right (from column 0 to column 1)                      |"
"\n"
"| up (u) - rotate up (floor 0 to floor 1)                                   |"
"\n"
"| down (j) - rotate down (floor 4 to floor 3)                               |"




"\n"
"| next (n) - move to next wall                                              |"
"\n"
"| back (m) - move to previous wall                                          |"
"\n"
"| done (d) - when 3D frame is done                                          |"
"\n"
"+---------------------------------------------------------------------------+"
"\n"
  );
}



/*---------------------------------------------------------------------------*/



void rotate_fb_forward(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  // Create new virtual framebuffer (image of original framebuffer)
  uint8_t i_fb_data_virtual_backup[5][5][5];

  // Index variables
  uint8_t i_wall;
  uint8_t i_led_level;
  uint8_t i_column;


  // Copy data to backup framebuffer
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        i_fb_data_virtual_backup[i_wall][i_led_level][i_column] =
            (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
      }
    }
  }

  // Do rotation - four cycles - last is "special"
  for(i_wall=0; i_wall<4 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        (*p_fb_data_virtual)[i_wall][i_led_level][i_column] =
            i_fb_data_virtual_backup[i_wall+1][i_led_level][i_column];
      }
    }
  }

  // And last cycle (data from WALL0 to WALL 4)
  for(i_led_level=0; i_led_level<5 ; i_led_level++)
  {
    for(i_column=0 ; i_column<5 ; i_column++)
    {
      (*p_fb_data_virtual)[4][i_led_level][i_column] =
          i_fb_data_virtual_backup[0][i_led_level][i_column];
    }
  }

  // OK, done :)
}



/*---------------------------------------------------------------------------*/



void rotate_fb_backward(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  // Create new virtual framebuffer (image of original framebuffer)
  uint8_t i_fb_data_virtual_backup[5][5][5];

  // Index variables
  uint8_t i_wall;
  uint8_t i_led_level;
  uint8_t i_column;


  // Copy data to backup framebuffer
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        i_fb_data_virtual_backup[i_wall][i_led_level][i_column] =
            (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
      }
    }
  }

  // Do rotation - 4 cycles - last is special
  for(i_wall=1 ; i_wall<5; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        (*p_fb_data_virtual)[i_wall][i_led_level][i_column] =
            i_fb_data_virtual_backup[i_wall-1][i_led_level][i_column];
      }
    }
  }

  // And last (special) rotation (WALL4 to WALL0)
  for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
  {
    for(i_column=0 ; i_column<5 ; i_column++)
    {
      (*p_fb_data_virtual)[0][i_led_level][i_column] =
          i_fb_data_virtual_backup[4][i_led_level][i_column];
    }
  }

  // Done
}



/*---------------------------------------------------------------------------*/



void rotate_fb_left(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  // Create new virtual framebuffer (image of original framebuffer)
  uint8_t i_fb_data_virtual_backup[5][5][5];

  // Index variables
  uint8_t i_wall;
  uint8_t i_led_level;
  uint8_t i_column;


  // Copy data to backup framebuffer
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        i_fb_data_virtual_backup[i_wall][i_led_level][i_column] =
            (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
      }
    }
  }

  // Do rotation 4 cycles (last is "special")
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<4 ; i_column++)
      {
        (*p_fb_data_virtual)[i_wall][i_led_level][i_column] =
            i_fb_data_virtual_backup[i_wall][i_led_level][i_column+1];
      }
    }
  }

  // Last rotation
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      (*p_fb_data_virtual)[i_wall][i_led_level][4] =
          i_fb_data_virtual_backup[i_wall][i_led_level][0];
    }
  }

  // Done
}



/*---------------------------------------------------------------------------*/



void rotate_fb_right(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  // Create new virtual framebuffer (image of original framebuffer)
  uint8_t i_fb_data_virtual_backup[5][5][5];

  // Index variables
  uint8_t i_wall;
  uint8_t i_led_level;
  uint8_t i_column;


  // Copy data to backup framebuffer
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        i_fb_data_virtual_backup[i_wall][i_led_level][i_column] =
            (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
      }
    }
  }

  // Do rotation - 4 cycles - last is special
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=1 ; i_column<5 ; i_column++)
      {
        (*p_fb_data_virtual)[i_wall][i_led_level][i_column] =
            i_fb_data_virtual_backup[i_wall][i_led_level][i_column-1];
      }
    }
  }

  // Special cycle
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
      (*p_fb_data_virtual)[i_wall][i_led_level][0] =
          i_fb_data_virtual_backup[i_wall][i_led_level][4];
  }

  // Done
}



/*---------------------------------------------------------------------------*/



void rotate_fb_up(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  // Create new virtual framebuffer (image of original framebuffer)
  uint8_t i_fb_data_virtual_backup[5][5][5];

  // Index variables
  uint8_t i_wall;
  uint8_t i_led_level;
  uint8_t i_column;


  // Copy data to backup framebuffer
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        i_fb_data_virtual_backup[i_wall][i_led_level][i_column] =
            (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
      }
    }
  }

  // Do rotation - 4 cycles - last is special
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=1 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        (*p_fb_data_virtual)[i_wall][i_led_level][i_column] =
            i_fb_data_virtual_backup[i_wall][i_led_level-1][i_column];
      }
    }
  }

  // And special cylcle
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_column=0 ; i_column<5 ; i_column++)
    {
      (*p_fb_data_virtual)[i_wall][0][i_column] =
          i_fb_data_virtual_backup[i_wall][4][i_column];
    }
  }
  // Done
}
/*---------------------------------------------------------------------------*/



void rotate_fb_down(uint8_t (*p_fb_data_virtual)[5][5][5])
{
  // Create new virtual framebuffer (image of original framebuffer)
  uint8_t i_fb_data_virtual_backup[5][5][5];

  // Index variables
  uint8_t i_wall;
  uint8_t i_led_level;
  uint8_t i_column;


  // Copy data to backup framebuffer
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<5 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        i_fb_data_virtual_backup[i_wall][i_led_level][i_column] =
            (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
      }
    }
  }

  // Do rotation - 4 cycles - last is special
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_led_level=0 ; i_led_level<4 ; i_led_level++)
    {
      for(i_column=0 ; i_column<5 ; i_column++)
      {
        (*p_fb_data_virtual)[i_wall][i_led_level][i_column] =
            i_fb_data_virtual_backup[i_wall][i_led_level+1][i_column];
      }
    }
  }

  // Last cycle
  for(i_wall=0 ; i_wall<5 ; i_wall++)
  {
    for(i_column=0 ; i_column<5 ; i_column++)
    {
      (*p_fb_data_virtual)[i_wall][4][i_column] =
          i_fb_data_virtual_backup[i_wall][0][i_column];
    }
  }

  // Done
}



/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/


