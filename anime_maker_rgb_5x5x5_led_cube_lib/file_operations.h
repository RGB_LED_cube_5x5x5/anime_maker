/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Operations with generated files
 *
 * This functions usually open file and add some data.
 */


#ifndef _file_operations_library
#define _file_operations_library

#include <inttypes.h>   // Data types
#include <stdio.h>      // Standard I/O
#include <stdlib.h>     // Standard library
#include <string.h>     // Library for string operations
#include "animation_codes.h"    // Animation codes

#include "simple_functions.h"

#include "settings.h"

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/

/**
 * \brief Create new files in "generated_anime_folder" and write headers
 *
 * Create files named by animation_name and write some common data.
 *
 * @param p_animation_name Pointer to text array where animation name is saved
 */
void create_files_and_fill_headers(char *p_animation_name);

/**
 * \brief Write start of animation code to file with animation
 * @param i_slowdown_factor Slowdown factor for animation
 * @param p_animation_name Pointer to text array where animation name is saved
 */
void write_start_of_animation(int i_slowdown_factor, char *p_animation_name);

/**
 * \brief Write data from p_anim_stream to file
 *
 * @param p_anim_stream Pointer to serial data stream
 * @param i_frame Inform where abort reading from stream (frame counter)
 * @param p_animation_name Pointer to p_animation_name. Need for file\n
 *  modification
 */
void write_content_to_file(uint16_t (*p_anim_stream)[max_size_anim_stream],
                           uint32_t i_frame,
                           char *p_animation_name);


/**
 * \brief Write end of animation.
 *
 * @param i_options Options for ending animation.
 * @param p_animation_name Pointer to text array where animation name is saved
 */
void write_end_of_animation(int i_options, char *p_animation_name);

#endif
