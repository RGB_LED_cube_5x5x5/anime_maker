/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Functions to get data from user
 *
 * Usually is need to check input data, so this functions do this
 */

#ifndef _ask_for_library
#define _ask_for_library

#include <inttypes.h>   // Data types
#include <stdio.h>      // Standard I/O
#include <string.h>     // Library for string operations

#include "settings.h"

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
/**
 * \brief Ask user for animation name and save it
 *
 * Data are saved thru pointer.
 *
 * @param p_animation_name Pointer to text array where animation name is saved
 */
void ask_for_anime_name(char *p_animation_name);

/**
 * \brief Get slowdown factor from user
 * \return Slowdown factor. Already checked
 */
int ask_for_slow_down_factor(void);

/**
 * \brief Ask user for PWM value
 * @param c_color This variable switch question (just change color)
 * @return Valid PWM value
 */
uint8_t ask_for_pwm(char c_color);

/**
 * \brief When user type exit, then is called this function
 * @return Return valid repeat_animation value
 */
uint8_t ask_for_repeat_animation(void);

/**
 * \brief Load input data. If there is string "cancel", then status is set to\n
 * non zero value
 *
 * @param p_input_char To this variable will be saved captured character
 * \param p_message Message to print
 * @return Zero if there is not "cancel" string
 */
uint8_t get_ascii_char(char *p_input_char, char *p_message);

/**
 * \brief Load input data. If there is string "cancel", then status is set to\n
 * non-zero value. Else all input string is written to p_input_string
 * @param p_input_string To this variable will be saved captured string. This\n
 *   string should be bigger than input command
 * @param p_message Message to print user
 * @return Zero if there is not "cancel" string
 */
uint8_t get_ascii_string(char *p_input_string, char *p_message);

/**
 * \brief Load 8bit number from command line
 *
 * Load input data as string and then check for validity
 *
 * \param p_variable_name Name for variable. For example: "slowdown factor"
 * \param i_min Minimum requested value
 * \param i_max Maximum requested value
 *
 * \return Valid 8bit value
 */
uint8_t get_8bit_number(char *p_variable_name, int i_min, int i_max);

#endif
