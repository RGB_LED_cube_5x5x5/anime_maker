/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Functions to get data from user
 *
 * Usually is need to check input data, so this functions do this
 */

#include "ask_for.h"


void ask_for_anime_name(char *p_animation_name){
  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| Hello and welcome! This is simple animation maker for RGB 5x5x5 LED cube. |"
"\n"
"| First i need to know name for your animation, so please type animation    |"
"\n"
"| name. Please DO NOT USE spaces!                                           |"
"\n"
"+---------------------------------------------------------------------------+"
"\n| Animation name: ");

  // get animation name
  int status = 0;

  while( status != 1)
  {
    // Load data from keyboard
    status = scanf("%s", p_animation_name );

    if( status != 1)    // If problem...
    {
      printf(""
"| !! Sorry, this can not be used as animation name :(                       |"
"\n"
"+xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx+"
"\n");

    }
  }


  printf(""
"+---------------------------------------------------------------------------+"
"\n");
}



/*---------------------------------------------------------------------------*/



int ask_for_slow_down_factor(void)
{
  int i_slowdown_factor = 0;

  printf(""
"| OK, now we need slowdown factor. Normally LED cube produce 25 fps. That   |"
"\n"
"| means we need 25 frames for 1 second. However, sometimes is not need to   |"
"\n"
"| have 25 fps. And for this is slowdown factor. If is set to 1, then LED    |"
"\n"
"| cube produce 25 fps. If is set to 2, then 12.5 fps and so on. Maximum     |"
"\n"
"| value should not exceed 255. So please type slowdown factor.              |"
"\n"
"+---------------------------------------------------------------------------+"
"\n"
  );

  i_slowdown_factor = get_8bit_number("Slowdown factor", 1, 255);

  return i_slowdown_factor;
}



/*---------------------------------------------------------------------------*/



uint8_t ask_for_pwm(char c_color)
{
  uint8_t i_pwm_value = 0;

  // Decide according color
  switch(c_color)
  {
  case 'R':
    i_pwm_value = get_8bit_number("PWM - RED", 0, 255); break;
  case 'r':
    i_pwm_value = get_8bit_number("PWM - RED", 0, 255); break;
  case 'G':
    i_pwm_value = get_8bit_number("PWM - GREEN", 0, 255); break;
  case 'g':
    i_pwm_value = get_8bit_number("PWM - GREEN", 0, 255); break;
  case 'B':
    i_pwm_value = get_8bit_number("PWM - BLUE", 0, 255); break;
  case 'b':
    i_pwm_value = get_8bit_number("PWM - BLUE", 0, 255); break;
  default: i_pwm_value = get_8bit_number("PWM", 0, 255);
  }

  return i_pwm_value;
}



/*---------------------------------------------------------------------------*/



uint8_t ask_for_repeat_animation(void)
{
  printf(""
"| Do you want repeat animation? If not, just type 0.                        |"
"\n");
  uint8_t i_repeat_animation = get_8bit_number("Repeat animation", 0, 255);

  return i_repeat_animation;
}



/*---------------------------------------------------------------------------*/



uint8_t get_ascii_char(char *p_input_char, char *p_message)
{
  printf("| %s: ", p_message);

  char c_input_data[default_text_len];

  uint8_t status = scanf("%s", c_input_data);

  if( (strcmp(c_input_data,"cancel")==0) ||
      (strcmp(c_input_data,"Cancel")==0) ||
      (strcmp(c_input_data,"CANCEL")==0) )
  {// If found cancel string
    *p_input_char = '\0';       // Write NULL character
    status = 1;
  }
  else
  {// Load first character and go away
    *p_input_char = c_input_data[0];
    status = 0;
  }

  return status;        // And exit
}



/*---------------------------------------------------------------------------*/



uint8_t get_ascii_string(char *p_input_string, char *p_message)
{
  printf("| %s: ", p_message);

  uint8_t status = scanf("%s", p_input_string);

  if( (strcmp(p_input_string,"cancel")==0) ||
      (strcmp(p_input_string,"Cancel")==0) ||
      (strcmp(p_input_string,"CANCEL")==0) )
  {// If found cancel string
    *p_input_string = '\0';     // Write NULL character to first cell in array
    status = 1;
  }
  else
  {// Load first character and go away
    status = 0;
  }

  return status;
}



/*---------------------------------------------------------------------------*/



uint8_t get_8bit_number(char *p_variable_name, int i_min, int i_max)
{
  int status = 0;

  int i_valid_number = 0;

  // Read data from keyboard as text... Little bit bigger then needed
  char c_input_data[default_text_len];

  printf("| %s: ", p_variable_name);

  while(status != 1)
  {
    // Load data from keyboard
    status = scanf("%s", c_input_data);

    status = 1; // Set status to 1 - so far no problem

    int i;
    // Scan input data - test if it is number, or not
    for(i=0 ; i<default_text_len ; i++)
    {   /* Test character - if null (last character) -> break -> in "i" is
         * length
         */
      if( c_input_data[i] == '\0' )
      {
        // Test if length is not zero
        if( i == 0)
        {       // If length is zero -> not a number
          status = 1;
        }
        break;
      }
      // Test character - if found anything else than 0~9 -> set status to "0"
      if( (c_input_data[i]<'0') || (c_input_data[i]>'9'))
      {
        status = 0;
      }
    }

    // If problem
    if( status != 1 )
    {
      printf(""
"| !! Sorry. this is not valid number. Please type number from %d to %d\n"
"| %s: ",
             i_min, i_max, p_variable_name);
    }
    else
    {// If there was no problem -> calculate input number
      int i_mul = 1;            // Multiplier for calculation
      i_valid_number = 0;    // Restart slowdown factor

      /* Load numbers backward, so it will be easy to calculate them. Variable
       * "i" is used as counter from last for cycle
       */
      for(i=i-1 ; i>=0 ; i--)
      {
        /* Calculate value of actual "number" stored in text array and add it
         * to previous result
         */
        i_valid_number = i_valid_number +
                            i_mul * ((c_input_data[i])-'0');
        i_mul = i_mul * 10;     // Multiply multiplier by 10 (decade)
      }
    }
  }

  // If user type number out of scale
  if( i_valid_number < i_min )
  {// If smaller than limit
    i_valid_number = i_min;      // Set minimum
    printf("| ! Warning: %s can not be smaller than %d -> set to %d\n",
           p_variable_name, i_min, i_min);
  }

  if( i_valid_number > i_max )
  {
    i_valid_number = i_max;
    printf("| ! Warning: %s can not be greater than %d -> set to %d\n",
           p_variable_name, i_max, i_max);
  }

  return (uint8_t)i_valid_number;
}



/*---------------------------------------------------------------------------*/




/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

