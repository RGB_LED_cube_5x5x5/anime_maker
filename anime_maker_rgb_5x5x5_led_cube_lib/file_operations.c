/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Operations with generated files
 *
 * This functions usually open file and add some data.
 */


#include "file_operations.h"



/*--------------------------------------------------------------------------*/



void create_files_and_fill_headers(char *p_animation_name)
{
  FILE *file;   // Pointer to file

  // String for complete path (include filename and extension -> +2)
  char c_file_and_path[name_len + path_len +2];


  // Save string contains name of anime folder
  strcpy(c_file_and_path, generated_anime_folder);
  // Add slash and prefix anim
  strcat(c_file_and_path, "/anim_");
  // Add file name
  strcat(c_file_and_path, p_animation_name);
  // Add ".h"
  strcat(c_file_and_path, ".h");

  // Open file - .h file (overwrite mode)
  file = fopen( c_file_and_path, "w+");

  char c_file_content[3000];

  // OK, now fill content for .h file
  strcpy(c_file_content, "\
/**\n\
 * \\file\n\
 *\n\
 * \\brief Animation: ");
  // Add animation name
  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, "\n * For RGB LED cube 5x5x5\n *\n");
  // Version
  strcat(c_file_content,
" * \\author Anime maker version ");
  strcat(c_file_content, version);

  strcat(c_file_content, "\n *\n"
" * Anime maker by: Martin Stejskal (martin.stej@gmail.com)\n"
" *\n\
 */\n\
\n\
#ifndef _anim_");
  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, "_library_\n#define _anim_");
  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, "_library_\n\n"
"/*-----------------------------------------*\n"
" |               Includes                  |\n"
" *-----------------------------------------*/\n"
"#include <avr/pgmspace.h>       // For operations with flash memory\n"
"#include <inttypes.h>           // Data types\n"
"\n"
"#include \"animation_codes.h\"    // For more \"human read\" animation code\n"
"\n"
"extern const uint16_t bin_anim_");
  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, "[];\n"
"\n"
"#endif\n");

  // Write content to file
  fprintf(file, "%s", c_file_content);

  // Close .h file
  fclose(file);



  // Save string contains name of anime folder
  strcpy(c_file_and_path, generated_anime_folder);
  // Add slash and prefix anim
  strcat(c_file_and_path, "/anim_");
  // Add file name
  strcat(c_file_and_path, p_animation_name);
  // Add ".c"
  strcat(c_file_and_path, ".c");

  // Open file - .c file (overwrite mode)
  file = fopen( c_file_and_path, "w+");

  // OK, now fill content for .c file
  strcpy(c_file_content, "\
/**\n\
 * \\file\n\
 *\n\
 * \\brief Animation: ");
  // Add animation name
  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, "\n * For RGB LED cube 5x5x5\n *");
  // Version
  strcat(c_file_content, "\n\
 * \\author Anime maker version ");
  strcat(c_file_content, version);

  strcat(c_file_content, "\n *\n"
" * Anime maker by: Martin Stejskal (martin.stej@gmail.com)\n"
" *\n"
" */\n"
"\n"
"#include \"anim_");
  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, ".h\"\n"
"\n"
"/* Data format (binary data for framebuffer)\n"
" *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|\n"
" * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n"
" * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |\n"
" * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n"
" *\n"
" * Instruction format (if MSB is 1, then it is possible change some settings)"
"\n *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|\n"
" * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n"
" * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |\n"
" * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n"
" *  C - command (0~127)\n"
" *  V - value   (0~255)\n"
" *\n"
" */\n"
"const uint16_t bin_anim_");

  strcat(c_file_content, p_animation_name);
  strcat(c_file_content, "[] PROGMEM =\n  {  // Serial stream of data\n");

  // Write content to file
  fprintf(file, "%s", c_file_content);

  // Close .c file
  fclose(file);
}



/*--------------------------------------------------------------------------*/



void write_start_of_animation(int i_slowdown_factor, char *p_animation_name)
{
  // Test for input parameters
  if( (i_slowdown_factor > 255) || (i_slowdown_factor < 0))
  {
    i_slowdown_factor = 0;      // If input variable is out of range
    printf(""
"| ! Warning: i_slowdown_factor out of range. Set to 0 (default)             |"
"\n"
        );
  }


  FILE *file;   // Pointer to file

  // String for complete path (include filename and extension -> +2)
  char c_file_and_path[name_len + path_len +2];


  // Save string contains name of anime folder
  strcpy(c_file_and_path, generated_anime_folder);
  // Add slash and prefix anim
  strcat(c_file_and_path, "/anim_");
  // Add file name
  strcat(c_file_and_path, p_animation_name);
  // Add ".c"
  strcat(c_file_and_path, ".c");

  // Open file - .c file (append mode)
  file = fopen( c_file_and_path, "a+");

  char c_file_content[256];

  strcpy(c_file_content, ""
"    // Command start of animation + value (slowdown factor)\n"
"    cmd_anim_start + "
  );

  // Transform i_slowdown_factor to string number
  char c_number_as_text[4];
  sprintf( c_number_as_text, "%d", i_slowdown_factor);

  strcat(c_file_content, c_number_as_text);
  strcat(c_file_content, ",\n    //Data + other commands (body)\n");

  // Write content to file
  fprintf(file, "%s", c_file_content);

  fclose(file);
}



/*--------------------------------------------------------------------------*/




void write_content_to_file(uint16_t (*p_anim_stream)[max_size_anim_stream],
                           uint32_t i_frame,
                           char *p_animation_name)
{
  // Open file...
  FILE *file;   // Pointer to file

  // String for complete path (include filename and extension -> +2)
  char c_file_and_path[name_len + path_len +2];

  // Save string contains name of anime folder
  strcpy(c_file_and_path, generated_anime_folder);
  // Add slash and prefix anim
  strcat(c_file_and_path, "/anim_");
  // Add file name
  strcat(c_file_and_path, p_animation_name);
  // Add ".c"
  strcat(c_file_and_path, ".c");

  // Open file - .c file (append mode)
  file = fopen( c_file_and_path, "a+");


  // Temporary text string (for writing "one line")
  char c_tmp_txt[512];

  fprintf(file, "    // Frame 0\n    "); // Write to file


  // Temporary string for transformation integer to string
  char c_tmp_num[10];

  // Counts written frame
  uint32_t i_actual_frame = 0;

  // Counts actual wall
  uint8_t  i_actual_wall = 0;

  // Counts actual column
  uint8_t  i_actual_column = 0;

  // Counts words in p_anim_stream (used index)
  uint64_t i_word_index = 0;

  // Word read from anime stream
  uint16_t i_anime_word;

  /* Inform if there were made any frame compression thru command -> if yes,
   * then do not test for other compression
   */
  uint8_t i_command_created = 0;

  /* Write frame-by-frame (include special commands) - until all frames are
   * written
   */
  while( i_actual_frame < i_frame )
  {
    // Read word
    i_anime_word = (*p_anim_stream)[i_word_index];

    // No command was created. At least for this cycle....
    i_command_created = 0;


    /* Test for cmd_draw_character. If yes, then no data processing needed ->
     * -> No testing for void frame and so on
     */
    if( ((*p_anim_stream)[i_word_index] & ( 0xFF00 ))
          == cmd_draw_character )
    {
      i_command_created = 1;
    }

    /* Test for cmd_end_of_3D_frame. If command found, then there must not be
     * done any compress operations (they can change data to command)
     */
    if( ((*p_anim_stream)[i_word_index] & 0xFF00)
          == cmd_end_of_3D_frame )
    {
      i_command_created = 1;
    }

    /* Test for cmd_rand_all_pwm. If this command is enabled -> skip some
     * frames -> no compress needed
     */
    if( ((*p_anim_stream)[i_word_index] & 0xFF00)
        == cmd_rand_all_pwm )
    {
      i_command_created = 1;
    }


    /* Test if actual 3D frame is void. If yes, then create special command.
     */
    if( (*p_anim_stream)[i_word_index+ 0] == 0 &&
        (*p_anim_stream)[i_word_index+ 1] == 0 &&
        (*p_anim_stream)[i_word_index+ 2] == 0 &&
        (*p_anim_stream)[i_word_index+ 3] == 0 &&
        (*p_anim_stream)[i_word_index+ 4] == 0 &&
        (*p_anim_stream)[i_word_index+ 5] == 0 &&
        (*p_anim_stream)[i_word_index+ 6] == 0 &&
        (*p_anim_stream)[i_word_index+ 7] == 0 &&
        (*p_anim_stream)[i_word_index+ 8] == 0 &&
        (*p_anim_stream)[i_word_index+ 9] == 0 &&
        (*p_anim_stream)[i_word_index+10] == 0 &&
        (*p_anim_stream)[i_word_index+11] == 0 &&
        (*p_anim_stream)[i_word_index+12] == 0 &&
        (*p_anim_stream)[i_word_index+13] == 0 &&
        (*p_anim_stream)[i_word_index+14] == 0 &&
        (*p_anim_stream)[i_word_index+15] == 0 &&
        (*p_anim_stream)[i_word_index+16] == 0 &&
        (*p_anim_stream)[i_word_index+17] == 0 &&
        (*p_anim_stream)[i_word_index+18] == 0 &&
        (*p_anim_stream)[i_word_index+19] == 0 &&
        (*p_anim_stream)[i_word_index+20] == 0 &&
        (*p_anim_stream)[i_word_index+21] == 0 &&
        (*p_anim_stream)[i_word_index+22] == 0 &&
        (*p_anim_stream)[i_word_index+23] == 0 &&
        (*p_anim_stream)[i_word_index+24] == 0 &&
        // Must be at begin of 3D frame
        (i_actual_column == 0) && (i_actual_wall == 0) &&
        (i_command_created == 0) )//&&
        // And previous command should not be cmd_end_of_3D_frame
    {
      i_anime_word = cmd_param_3D_frame + param_3D_frame_void;
      i_word_index = i_word_index +24;
      i_actual_wall = 5;        /* Overflow -> when process command, then
                                 * do necessary routine
                                 */

      i_command_created = 1;
    }


    /* Test if actual 3D frame is full of ones. If yes, then create special
     * command.
     */
    if( (*p_anim_stream)[i_word_index+ 0] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 1] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 2] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 3] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 4] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 5] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 6] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 7] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 8] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+ 9] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+10] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+11] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+12] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+13] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+14] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+15] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+16] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+17] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+18] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+19] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+20] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+21] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+22] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+23] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+24] == 0x7FFF &&
        // Must be at begin of 3D frame
        (i_actual_column == 0) && (i_actual_wall == 0) &&
        (i_command_created == 0))
    {
      i_anime_word = cmd_param_3D_frame + param_3D_frame_ones;
      i_word_index = i_word_index +24;
      i_actual_wall = 5;        /* Overflow -> when process command, then
                                 * do necessary routine
                                 */
      i_command_created = 1;
    }


    /* Test if actual 3D frame is same as previous. If yes, then create special
     * command.
     */
    if(
      (*p_anim_stream)[i_word_index+ 0] == (*p_anim_stream)[i_word_index-25] &&
      (*p_anim_stream)[i_word_index+ 1] == (*p_anim_stream)[i_word_index-24] &&
      (*p_anim_stream)[i_word_index+ 2] == (*p_anim_stream)[i_word_index-23] &&
      (*p_anim_stream)[i_word_index+ 3] == (*p_anim_stream)[i_word_index-22] &&
      (*p_anim_stream)[i_word_index+ 4] == (*p_anim_stream)[i_word_index-21] &&
      (*p_anim_stream)[i_word_index+ 5] == (*p_anim_stream)[i_word_index-20] &&
      (*p_anim_stream)[i_word_index+ 6] == (*p_anim_stream)[i_word_index-19] &&
      (*p_anim_stream)[i_word_index+ 7] == (*p_anim_stream)[i_word_index-18] &&
      (*p_anim_stream)[i_word_index+ 8] == (*p_anim_stream)[i_word_index-17] &&
      (*p_anim_stream)[i_word_index+ 9] == (*p_anim_stream)[i_word_index-16] &&
      (*p_anim_stream)[i_word_index+10] == (*p_anim_stream)[i_word_index-15] &&
      (*p_anim_stream)[i_word_index+11] == (*p_anim_stream)[i_word_index-14] &&
      (*p_anim_stream)[i_word_index+12] == (*p_anim_stream)[i_word_index-13] &&
      (*p_anim_stream)[i_word_index+13] == (*p_anim_stream)[i_word_index-12] &&
      (*p_anim_stream)[i_word_index+14] == (*p_anim_stream)[i_word_index-11] &&
      (*p_anim_stream)[i_word_index+15] == (*p_anim_stream)[i_word_index-10] &&
      (*p_anim_stream)[i_word_index+16] == (*p_anim_stream)[i_word_index- 9] &&
      (*p_anim_stream)[i_word_index+17] == (*p_anim_stream)[i_word_index- 8] &&
      (*p_anim_stream)[i_word_index+18] == (*p_anim_stream)[i_word_index- 7] &&
      (*p_anim_stream)[i_word_index+19] == (*p_anim_stream)[i_word_index- 6] &&
      (*p_anim_stream)[i_word_index+20] == (*p_anim_stream)[i_word_index- 5] &&
      (*p_anim_stream)[i_word_index+21] == (*p_anim_stream)[i_word_index- 4] &&
      (*p_anim_stream)[i_word_index+22] == (*p_anim_stream)[i_word_index- 3] &&
      (*p_anim_stream)[i_word_index+23] == (*p_anim_stream)[i_word_index- 2] &&
      (*p_anim_stream)[i_word_index+24] == (*p_anim_stream)[i_word_index- 1] &&
        // Must be at begin of 3D frame - and it could NOT be frame 0
        (i_actual_column == 0) && (i_actual_wall == 0) && (i_actual_frame > 0))
    {
      i_anime_word = cmd_param_3D_frame + param_3D_frame_same;
      i_word_index = i_word_index +24;
      i_actual_wall = 5;        /* Overflow -> when process command, then
                                 * do necessary routine
                                 */
      i_command_created = 1;
    }










    /* However, test if there is void 2D frame. If yes, then change
     * i_anime_word into command and, of course, increase i_word_index
     */
    if( (*p_anim_stream)[i_word_index+0] == 0x0000 &&
        (*p_anim_stream)[i_word_index+1] == 0x0000 &&
        (*p_anim_stream)[i_word_index+2] == 0x0000 &&
        (*p_anim_stream)[i_word_index+3] == 0x0000 &&
        (*p_anim_stream)[i_word_index+4] == 0x0000 &&
        (i_actual_column == 0) &&       // Must be at the begin of 2D frame
        (i_command_created == 0))
    {
      i_anime_word = cmd_param_2D_frame + param_2D_frame_void;
      i_word_index = i_word_index +4;   // And move to the next 2D frame

      // Move to next wall
      i_actual_wall++;

      i_command_created = 1;
    }


    /* Again, test if there is full 2D frame. If yes, then change
     * i_anime_word into command and, of course, increase i_word_index
     */
    if( (*p_anim_stream)[i_word_index+0] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+1] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+2] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+3] == 0x7FFF &&
        (*p_anim_stream)[i_word_index+4] == 0x7FFF &&
        (i_actual_column == 0) &&       // Must be at the begin of 2D frame
        (i_command_created == 0))
    {
      i_anime_word = cmd_param_2D_frame + param_2D_frame_ones;
      i_word_index = i_word_index +4;   // And move to the next 2D frame

      // Move to next wall
      i_actual_wall++;

      i_command_created = 1;
    }


    /* Test if actual frame is same as previous. Can work only for WALL1~WALL4.
     * Again change i_anime_word into command and, of course, increase
     * i_word_index
     */
    if( (*p_anim_stream)[i_word_index-5] == (*p_anim_stream)[i_word_index+0] &&
        (*p_anim_stream)[i_word_index-4] == (*p_anim_stream)[i_word_index+1] &&
        (*p_anim_stream)[i_word_index-3] == (*p_anim_stream)[i_word_index+2] &&
        (*p_anim_stream)[i_word_index-2] == (*p_anim_stream)[i_word_index+3] &&
        (*p_anim_stream)[i_word_index-1] == (*p_anim_stream)[i_word_index+4] &&
        // Must be at the begin of 2D frame and WALL must be greater than WALL0
        (i_actual_column == 0) && (i_actual_wall > 0) &&
        (i_command_created == 0))
    {
      i_anime_word = cmd_param_2D_frame + param_2D_frame_same;
      i_word_index = i_word_index +4;   // And move to the next 2D frame

      // Move to next wall
      i_actual_wall++;

      i_command_created = 1;
    }



    // Read word from stream -> decide if it is command or value
    if( ( i_anime_word & (0x8000) ) == 0  )
    {// If zero (use AND masking) -> data
      // Transform integer to string (HEX format)
      sprintf( c_tmp_num, "0x%04X,", i_anime_word);

      // Write number
      fprintf(file, "%s", c_tmp_num);

      i_actual_column++;

      // Test if we are on the last column
      if(i_actual_column > 4)
      {// Write line to file ; increase i_actual_wall
        fprintf(file, "\n    ");        // Write to file newline

        i_actual_column = 0;            // Set new active column to 0

        // Move to next wall
        i_actual_wall++;
        // Test if we on the last wall
        if(i_actual_wall > 4)
        {// If last wall -> write 2 newline ; increase i_actual_frame
          i_actual_frame++;
          i_actual_wall = 0;            // Set actual wall to 0

          // Write to file 2 newlines and add info about frame #
          fprintf(file, "\n\n    // Frame %d\n    ", i_actual_frame);
        }
      }
    }
    else
    {// Else word is command
      /* Because some commands has as their value not just number, but for
       * easier reading animation code it is nice to write symbolic value
       * (defined in animation_codes.h)
       */
      uint8_t i_value_added = 0;

      // Extract just code number and then decide....
      switch( ((i_anime_word) & 0xFF00) )
      {
      // CMD PWM
      case cmd_set_pwm_r: strcpy( c_tmp_txt, "cmd_set_pwm_r"); break;
      case cmd_set_pwm_g: strcpy( c_tmp_txt, "cmd_set_pwm_g"); break;
      case cmd_set_pwm_b: strcpy( c_tmp_txt, "cmd_set_pwm_b"); break;

      // CMD PARAM 2D FRAME
      case cmd_param_2D_frame: strcpy( c_tmp_txt, "cmd_param_2D_frame + ");
        switch( (i_anime_word)&(0xFF) )
        {
        case param_2D_frame_void:
          strcat( c_tmp_txt, "param_2D_frame_void"); break;
        case param_2D_frame_same:
          strcat( c_tmp_txt, "param_2D_frame_same"); break;
        case param_2D_frame_ones:
          strcat( c_tmp_txt, "param_2D_frame_ones"); break;
        }
        i_value_added = 1;
      break;    // cmd_param_2D_frame

      // CMD PARAM 3D FRAME
      case cmd_param_3D_frame: strcpy( c_tmp_txt, "cmd_param_3D_frame + ");
        switch( (i_anime_word)&(0xFF) )
        {
        case param_3D_frame_void:
          strcat( c_tmp_txt, "param_3D_frame_void"); break;
        case param_3D_frame_same:
          strcat( c_tmp_txt, "param_3D_frame_same"); break;
        case param_3D_frame_ones:
          strcat( c_tmp_txt, "param_3D_frame_ones"); break;
        }
        i_value_added = 1;
      break;    // cmd_param_3D_frame

      // CMD DRAW CHARACTER
      case cmd_draw_character: strcpy( c_tmp_txt, "cmd_draw_character"); break;

      // CMD ROTATE
      case cmd_rotate_fb: strcpy( c_tmp_txt, "cmd_rotate_fb + ");
        switch( (i_anime_word)&(0xFF))
        {
        case param_rotate_fb_forward:
          strcat( c_tmp_txt, "param_rotate_fb_forward"); break;
        case param_rotate_fb_backward:
          strcat( c_tmp_txt, "param_rotate_fb_backward"); break;
        case param_rotate_fb_left:
          strcat( c_tmp_txt, "param_rotate_fb_left"); break;
        case param_rotate_fb_right:
          strcat( c_tmp_txt, "param_rotate_fb_right"); break;
        case param_rotate_fb_up:
          strcat( c_tmp_txt, "param_rotate_fb_up"); break;
        case param_rotate_fb_down:
          strcat( c_tmp_txt, "param_rotate_fb_down"); break;
        }
        i_value_added = 1;
      break;    // cmd_rotate_fb


      // CMD RAND ALL PWM
      case cmd_rand_all_pwm:
        strcpy( c_tmp_txt, "cmd_rand_all_pwm");
        i_actual_frame = i_actual_frame + (i_anime_word & 0x00FF);
        i_actual_wall = 5;      // Overflow -> when testing -> do other
      break;    // cmd_rand_all_pwm


      // CMD END OF 3D FRAME
      case cmd_end_of_3D_frame:
        strcpy( c_tmp_txt, "cmd_end_of_3D_frame");
        i_actual_wall = 5;      // Overflow -> when testing -> do other

      break;    // cmd_end_of_3D_frame

      // Case, that command not found -> all to default
      default: strcpy( c_tmp_txt, "0x8000, // Command not found");
      }

      // Add value and some things
      // Add value to the code
      //sprintf( c_tmp_num, " + %d", (i_anime_word)&(0xFF)>>8 );

      if( i_value_added == 0)
      {// If value was not added -> add it just like normal value
        // So, now just write it to the file - value is written low 8 bits
        fprintf(file, "%s + %d,\n    ", c_tmp_txt, (i_anime_word)&(0xFF) );
      }
      else
      {// Else value was already written (as symbolic name)
        fprintf(file, "%s,\n    ", c_tmp_txt);
      }


      /* Test if we were on the last wall (for case,that was found compress
       * command -> increase wall -> now check       *
       */
      if(i_actual_wall > 4)
      {// If last wall -> write 2 newline ; increase i_actual_frame
        i_actual_frame++;
        i_actual_wall = 0;            // Set actual wall to 0

        // Write to file 2 newlines and add info about frame #
        fprintf(file, "\n\n    // Frame %d\n    ", i_actual_frame);
      }


    }


    // Move to next word
    i_word_index++;
  }


  // Close file
  fclose(file);
}



/*--------------------------------------------------------------------------*/




void write_end_of_animation(int i_options, char *p_animation_name)
{
  FILE *file;   // Pointer to file

  // String for complete path (include filename and extension -> +2)
  char c_file_and_path[name_len + path_len +2];


  // Save string contains name of anime folder
  strcpy(c_file_and_path, generated_anime_folder);
  // Add slash and prefix anim
  strcat(c_file_and_path, "/anim_");
  // Add file name
  strcat(c_file_and_path, p_animation_name);
  // Add ".c"
  strcat(c_file_and_path, ".c");

  // Open file - .c file (append mode)
  file = fopen( c_file_and_path, "a+");

  char c_file_content[128];

  strcpy(c_file_content, "\n"
"    // End of animation + option\n"
"    cmd_end_of_anim + "
  );

  char c_number_as_text[4];
  sprintf( c_number_as_text, "%d", i_options);

  // Transform i_slowdown_factor to string number
  strcat(c_file_content, c_number_as_text);
  strcat(c_file_content, "\n  };");

  // Write content to file
  fprintf(file, "%s", c_file_content);

  fclose(file);
}


