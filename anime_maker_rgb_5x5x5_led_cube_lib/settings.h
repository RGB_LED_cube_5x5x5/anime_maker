/**
 * \file
 *
 * \brief Additional settings for anime maker
 *
 * \author Martin Stejskal
 *
 */

// Trick for multiply include this file
#ifndef _settings_library_
#define _settings_library_

/**
 * \brief Version
 */
#define version         "1.0 beta"

/**
 * \brief Maximal name length
 */
#define name_len        83

/**
 * \brief Maximal name for animation folder (usually generated_animations)
 * @param p_animation_name
 */
#define path_len        25

/**
 * \brief
 */
#define generated_anime_folder          "generated_animations"


/**
 * \brief Default length for input text/variable
 */
#define default_text_len        81


/**
 * \brief Return code when no error
 */
#define no_error                0

/**
 * \brief Return code when error on input
 */
#define incorrect_input         1


/**
 * \brief Value for function jump_out_to_next_screen
 *
 * Before new "image" is printed, function jump_out_to_next_screen print\n
 * a lots of line feed characters, so it appear, that there is "clean"\n
 * screen. And this constant define how many times is line feed printed.\n
 * If there is number 1, then 10 times is printed line feed. If there is 20\n
 * then line feed is printed 200 times.
 */
#define screen_lines_max        1


/**
 * \brief Time period between two frames (in "full" speed od LED cube)
 *
 * This value is for virtual timer. It should be same as in firmware on LED\n
 * cube. However, this time is just for user information
 */
#define frame_period            0.040

/**
 * \brief Maximal length of animation (bytes)
 */
#define max_size_anim_stream    128000

/**
 * \brief Maximum commands per 1 frame (3D frame)
 */
#define max_commands_per_frame  250

/**
 * \brief Maximum length input command
 */
#define max_command_length      30

/**
 * \brief Binary shift for colors
 *
 * This define position of colors in virtual framebuffer
 */
#define red_shift               2
#define green_shift             1
#define blue_shift              0

/**
 * \brief If pressed button, then cursor will not be changed automatic
 */
#define pressed_button          126


#endif
