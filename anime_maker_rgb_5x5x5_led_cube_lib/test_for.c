/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief When user type command, some functions must process it
 *
 * This is set of functions, that test input command and if match, then do\n
 * their data process
 */


#include "test_for.h"



/*---------------------------------------------------------------------------*/



void test_for_EXIT(char *p_cmd, int *p_animation_done)
{
  if( (strcmp(p_cmd,"exit")==0) ||
      (strcmp(p_cmd,"Exit")==0) ||
      (strcmp(p_cmd,"EXIT")==0) ||
      (strcmp(p_cmd,"e")   ==0) )
  {// Exit - everything is done. Just break while condition
    *p_animation_done = 1;
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_HELP(char *p_cmd)
{
  if( (strcmp(p_cmd,"help")==0) ||
      (strcmp(p_cmd,"Help")==0) ||
      (strcmp(p_cmd,"HELP")==0) ||
      (strcmp(p_cmd,"h")   ==0) )
  {
    printf(""
"\n"
"+---------------------------------------------------------------------------+"
"\n"
"|  exit (e) - save data (to actual frame) and exit from application         |"
"\n"
"|  zeros - clear actual frame - turn off all LEDs on actual frame           |"
"\n"
"|  ones - set actual frame - turn on all LEDs on acutal frame               |"
"\n"
"|  next (n) - move to next wall. Actual 2D frame will be saved              |"
"\n"
"|  back (m) - move to previous wall. Actual 2D frame will be saved          |"
"\n"
"|  pwmr - set PWM on channel with RED LEDs. pwmg is for green and pwmb for  |"
"\n"
"|         blue channel. Value of PWM is same for whole frame.               |"
"\n"
"|  line (l) - copy actual segment thru line                                 |"
"\n"
"|  column (c)- copy actual segment thru column                              |"
"\n"
"|  font (f) - use defined font and load character to WALL0. After this      |"
"\n"
"|             this command user can enter only following commands: cancel,  |"
"\n"
"|             rotate, next, back and done                                   |"
"\n"
"|  rotate (t) - this command allow do rotation with framebuffer.            |"
"\n"
"|  randallpwm - set all PWM channels randomly                               |"
"\n"
"|  cancel - escape from actual \"mode\". Data in framebuffer will be saved"
"    |"
"\n"
"|           normal data, not like command!                                  |"
"\n"
"+---------------------------------------------------------------------------+"
"\n"
"| Press enter to close this page                                            |"
"\n"
"+---------------------------------------------------------------------------+"
    );

    wait_for_user();

    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_ZEROS(char *p_cmd,
                    uint8_t  (*p_fb_data_virtual)[5][5][5],
                    uint8_t i_wall)
{
  if( (strcmp(p_cmd,"zeros")==0) ||
      (strcmp(p_cmd,"Zeros")==0) ||
      (strcmp(p_cmd,"ZEROS")==0) )
  {
    for(int j=0 ; j<5 ; j++)
    {
      for(int k=0 ; k<5 ; k++)
      {
        (*p_fb_data_virtual)[i_wall][j][k] = 0;
      }
    }
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_ONES(char *p_cmd,
    uint8_t  (*p_fb_data_virtual)[5][5][5],
    uint8_t i_wall)
{
  if( (strcmp(p_cmd,"ones")==0) ||
      (strcmp(p_cmd,"Ones")==0) ||
      (strcmp(p_cmd,"ONES")==0) )
  {
    for(int j=0 ; j<5 ; j++)
    {
      for(int k=0 ; k<5 ; k++)
      {
       (*p_fb_data_virtual)[i_wall][j][k] = (1<<red_shift)|
                                            (1<<green_shift)|
                                            (1<<blue_shift);
      }
    }
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_PWM_RED(char *p_cmd,
                      uint8_t *p_pwm_r,
                      uint16_t (*p_commands)[max_commands_per_frame],
                      uint16_t *p_command_index)
{
  if( (strcmp(p_cmd,"pwmr")==0) ||
      (strcmp(p_cmd,"Pwmr")==0) ||
      (strcmp(p_cmd,"PWMR")==0) )
  {// And ask for value
    *p_pwm_r = ask_for_pwm('R');
    // And create command
    (*p_commands)[(*p_command_index)++] = cmd_set_pwm_r + *p_pwm_r;
    clear_loaded_command( p_cmd );
  }
}




/*---------------------------------------------------------------------------*/



void test_for_PWM_GREEN(char *p_cmd,
                      uint8_t *p_pwm_g,
                      uint16_t (*p_commands)[max_commands_per_frame],
                      uint16_t *p_command_index)
{
  if( (strcmp(p_cmd,"pwmg")==0) ||
      (strcmp(p_cmd,"Pwmg")==0) ||
      (strcmp(p_cmd,"PWMG")==0) )
  {// And ask for value
    *p_pwm_g = ask_for_pwm('G');
    // And create command
    (*p_commands)[(*p_command_index)++] = cmd_set_pwm_g + *p_pwm_g;
    clear_loaded_command( p_cmd );
  }
}


/*---------------------------------------------------------------------------*/



void test_for_PWM_BLUE(char *p_cmd,
                      uint8_t *p_pwm_b,
                      uint16_t (*p_commands)[max_commands_per_frame],
                      uint16_t *p_command_index)
{
  if( (strcmp(p_cmd,"pwmb")==0) ||
      (strcmp(p_cmd,"Pwmb")==0) ||
      (strcmp(p_cmd,"PWMB")==0) )
  {// And ask for value
    *p_pwm_b = ask_for_pwm('B');
    // And create command
    (*p_commands)[(*p_command_index)++] = cmd_set_pwm_b + *p_pwm_b;
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_RANDOM_ALL_PWM(char    *p_cmd,
                             uint16_t (*p_commands)[max_commands_per_frame],
                             uint16_t *p_command_index,
                             uint32_t *p_frame,
                             uint8_t  (*p_fb_data_virtual)[5][5][5],
                             uint16_t (*p_anim_stream)[max_size_anim_stream],
                             int      *p_slowdown_factor,
                             uint8_t  *p_wall,
                             float    *p_virtual_time )
{
  if( (strcmp(p_cmd,"randallpwm")==0) ||
      (strcmp(p_cmd,"Randallpwm")==0) ||
      (strcmp(p_cmd,"RANDALLPWM")==0) )
  {// Ask for value
    uint8_t i_repeat_factor =
        get_8bit_number("Random all pwm - repeat", 0, 255);

    // Create command
    (*p_commands)[(*p_command_index)++] = cmd_rand_all_pwm + i_repeat_factor;



    (*p_commands)[*p_command_index] = 0;      // On the last position write 0
    // And load data to "real buffer"
    write_frame(p_fb_data_virtual,
                p_anim_stream,
                *p_commands,
                *p_frame,
                0);

    *p_command_index = 0;    // After write reset
    *p_wall=0;
    // Every repeat is for one frame
    *p_frame = *p_frame + i_repeat_factor +1;

    // Recalculate time
    *p_virtual_time = *p_frame * (frame_period*(*p_slowdown_factor));

    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_LINE(char *p_cmd,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint8_t i_wall,
                   uint8_t i_led_level,
                   uint8_t i_column)
{
  if( (strcmp(p_cmd,"line")==0) ||
      (strcmp(p_cmd,"Line")==0) ||
      (strcmp(p_cmd,"LINE")==0) ||
      (strcmp(p_cmd,"l")   ==0) )
  {
    // Grab actual segment and copy it
    uint8_t i_segment = (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
    // And copy it thru actual 2D frame
    for(int i=0 ; i<5 ; i++)
    {
      (*p_fb_data_virtual)[i_wall][i_led_level][i] = i_segment;
    }
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_COLUMN(char *p_cmd,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint8_t i_wall,
                   uint8_t i_led_level,
                   uint8_t i_column)
{
  if( (strcmp(p_cmd,"column")==0) ||
      (strcmp(p_cmd,"Column")==0) ||
      (strcmp(p_cmd,"COLUMN")==0) ||
      (strcmp(p_cmd,"c")     ==0) )
  {
    // Grab actual segment and copy it
    uint8_t i_segment = (*p_fb_data_virtual)[i_wall][i_led_level][i_column];
    // And copy it thru actual 2D frame
    for(int i=0 ; i<5 ; i++)
    {
      (*p_fb_data_virtual)[i_wall][i][i_column] = i_segment;
    }
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_ARROW_UP(char (*p_cmd)[max_command_length],
                       uint8_t *p_led_level)
{
  if( (*p_cmd)[0]==27 && (*p_cmd)[1]==91 && (*p_cmd)[2]==65 && (*p_cmd)[3]==0)
  {
    (*p_led_level)++;           // Increase LED level
    if(*p_led_level>4)          // If greater than 4 (should not be)
    {
      *p_led_level=0;           // Set to zero
    }
    clear_loaded_command( *p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_ARROW_DOWN(char (*p_cmd)[max_command_length],
                         uint8_t *p_led_level)
{
  if( (*p_cmd)[0]==27 && (*p_cmd)[1]==91 && (*p_cmd)[2]==66 && (*p_cmd)[3]==0)
  {
    (*p_led_level)--;           // Decrease LED level
    if(*p_led_level==255)       // If smaller than 0 (should not be)
    {
      *p_led_level=4;           // Set to highest level again
    }
    clear_loaded_command( *p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_ARROW_RIGHT(char (*p_cmd)[max_command_length],
                         uint8_t *p_column)
{
  if( (*p_cmd)[0]==27 && (*p_cmd)[1]==91 && (*p_cmd)[2]==67 && (*p_cmd)[3]==0)
  {
    (*p_column)++;
    if(*p_column>4)
    {
      *p_column = 0;
    }
    clear_loaded_command( *p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_ARROW_LEFT(char (*p_cmd)[max_command_length],
                         uint8_t *p_column)
{
  if( (*p_cmd)[0]==27 && (*p_cmd)[1]==91 && (*p_cmd)[2]==68 && (*p_cmd)[3]==0)
  {
    (*p_column)--;
    if(*p_column==255)
    {
      *p_column = 4;
    }
    clear_loaded_command( *p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_NEXT(char     *p_cmd,
                   uint8_t  *p_column,
                   uint8_t  *p_led_level,
                   uint8_t  *p_wall,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor,
                   uint8_t  i_do_not_write_data)
{
  if( (strcmp(p_cmd,"next")==0) ||
      (strcmp(p_cmd,"Next")==0) ||
      (strcmp(p_cmd,"NEXT")==0) ||
      (strcmp(p_cmd,"n")   ==0) )
  {
    *p_column = 0;
    *p_led_level = 4;
    (*p_wall)++;       // Move to next wall
    // And check wall
    if(*p_wall>4)
    {
      (p_commands)[*p_command_index] = 0;      // On the last position write 0
      // And load data to "real buffer"
      write_frame(p_fb_data_virtual,
                  p_anim_stream,
                  p_commands,
                  *p_frame,
                  i_do_not_write_data);

      *p_command_index = 0;    // After write reset
      *p_wall=0;
      (*p_frame)++;    // Next frame -> increase
      // Recalculate time
      *p_virtual_time = *p_frame * (frame_period*(*p_slowdown_factor));
    }
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_BACK(char    *p_cmd,
                   uint8_t *p_column,
                   uint8_t *p_led_level,
                   uint8_t *p_wall)
{
  if( (strcmp(p_cmd,"back")==0) ||
      (strcmp(p_cmd,"Back")==0) ||
      (strcmp(p_cmd,"BACK")==0) ||
      (strcmp(p_cmd,"m")   ==0) )
  {
    *p_column = 0;
    *p_led_level = 4;
    (*p_wall)--;         // Move to previous wall
    // And check wall
    if(*p_wall>4)
    {
      *p_wall = 0;
      /**
       * \todo Complete back function (even for 3D frames)
       */
    }
    clear_loaded_command( p_cmd );
  }
}



/*---------------------------------------------------------------------------*/



void test_for_DATA(char   (*p_cmd)[max_command_length],
                   uint8_t  *p_column,
                   uint8_t  *p_led_level,
                   uint8_t  *p_wall,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor,
                   uint8_t  i_do_not_write_data)
{
  if((((*p_cmd)[0]=='A' || (*p_cmd)[0]=='a' || (*p_cmd)[0]=='1' ||
       (*p_cmd)[0]=='R' || (*p_cmd)[0]=='G' || (*p_cmd)[0]=='B' ||
       (*p_cmd)[0]=='r' || (*p_cmd)[0]=='g' || (*p_cmd)[0]=='b' ||
       (*p_cmd)[0]=='0' || (*p_cmd)[0]=='-') ||

      ((*p_cmd)[1]=='1' ||
       (*p_cmd)[1]=='R' || (*p_cmd)[1]=='G' || (*p_cmd)[1]=='B' ||
       (*p_cmd)[1]=='r' || (*p_cmd)[1]=='g' || (*p_cmd)[1]=='b' ||
       (*p_cmd)[1]=='0' || (*p_cmd)[1]=='-') ||

       ((*p_cmd)[2]=='1' ||
        (*p_cmd)[2]=='R' || (*p_cmd)[2]=='G' || (*p_cmd)[2]=='B' ||
        (*p_cmd)[2]=='r' || (*p_cmd)[2]=='g' || (*p_cmd)[2]=='b' ||
        (*p_cmd)[2]=='0' || (*p_cmd)[2]=='-')) &&

        // If some arrow pressed, then there can be symbol A,B,C or D, so...
       ( (*p_cmd)[0]!=27 ) && ((*p_cmd)[1]!=91) && ((*p_cmd)[3]!=27) &&
       ((*p_cmd)[4]!=91) &&
       ((*p_cmd)[0]!='\0'))
  {
    // Write data to buffer
    (*p_fb_data_virtual)[*p_wall][*p_led_level][*p_column] = 0;    // Set to 0

    // And now add data
    // Test if was set red color
    if( (*p_cmd)[0]=='r' || (*p_cmd)[0]=='R' || (*p_cmd)[0]=='a' ||
        (*p_cmd)[0]=='A' ||
        (*p_cmd)[1]=='r' || (*p_cmd)[1]=='R' || (*p_cmd)[0]=='1' ||
        (*p_cmd)[2]=='r' || (*p_cmd)[2]=='R' )
    {// If was set red bit -> add it
      (*p_fb_data_virtual)[*p_wall][*p_led_level][*p_column] |= (1<<red_shift);
    }

    // Test for "green bit"
    if( (*p_cmd)[0]=='g' || (*p_cmd)[0]=='G' || (*p_cmd)[0]=='a' ||
        (*p_cmd)[0]=='A' ||
        (*p_cmd)[1]=='g' || (*p_cmd)[1]=='G' || (*p_cmd)[1]=='1' ||
        (*p_cmd)[2]=='g' || (*p_cmd)[2]=='G' )
    {// If was set red bit -> add it
      (*p_fb_data_virtual)[*p_wall][*p_led_level][*p_column] |= (1<<green_shift);
    }
    // And test for "blue bit"
    if( (*p_cmd)[0]=='b' || (*p_cmd)[0]=='B' || (*p_cmd)[0]=='a' ||
        (*p_cmd)[0]=='A' ||
        (*p_cmd)[1]=='b' || (*p_cmd)[1]=='B' || (*p_cmd)[2]=='1' ||
        (*p_cmd)[2]=='b' || (*p_cmd)[2]=='B' )
    {// If was set red bit -> add it
      (*p_fb_data_virtual)[*p_wall][*p_led_level][*p_column] |= (1<<blue_shift);
    }


    // Change index
    //(*p_column)++;       // Increase column number

/*
    if(*p_column>4)
    {
      *p_column=0;     // Set to "first" colummn

      (*p_led_level)--;    // Decrease LED level
      if(*p_led_level==255) // If smaller than 0 (should not be)
      {
        *p_led_level=4;  // Set to highest level again
        (*p_wall)++;       // Move to next wall
        // And check wall
        if(*p_wall>4)
        {
          (p_commands)[*p_command_index] = 0;  // On the last position write 0
          // And load data to "real buffer"
          write_frame(p_fb_data_virtual,
                      p_anim_stream,
                      p_commands,
                      *p_frame,
                      i_do_not_write_data);

          *p_wall=0;

          *p_command_index = 0;    // After write reset command index

          (*p_frame)++;    // Next frame -> increase
          // Recalculate time
          *p_virtual_time =
              *p_virtual_time + (frame_period*(*p_slowdown_factor));
        }
      }
    }
    */
    clear_loaded_command( *p_cmd );
  }
}




/*---------------------------------------------------------------------------*/



void test_for_FONT(char *p_cmd,
                   uint8_t  *p_wall,
                   uint8_t  *p_pwm_r,
                   uint8_t  *p_pwm_g,
                   uint8_t  *p_pwm_b,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor)
{
  if( (strcmp(p_cmd,"font")==0) ||
      (strcmp(p_cmd,"Font")==0) ||
      (strcmp(p_cmd,"FONT")==0) ||
      (strcmp(p_cmd,"f")   ==0) )
  {// Ask for ASCII character
    uint8_t i_status = get_ascii_char( p_cmd, "Type character");
    if( i_status == 0)
    {// 0 -> character (not cancel)
      // Backup index
      uint16_t i_backup_command_index = (*p_command_index);

      // Write command to array
      p_commands[(*p_command_index)++] = cmd_draw_character + (p_cmd)[0];

      // Load character to framebuffer
      load_character_to_framebuffer( (p_cmd)[0], p_fb_data_virtual );

      *p_wall = 0;

      clear_loaded_command( p_cmd );

      // Infinite loop - break if user type "cancel" or "done"
      while(i_status == no_cancel_or_done_command)
      {
        jump_out_to_next_screen();
        // Refresh screen
        show_preview( p_fb_data_virtual, *p_command_index, *p_frame, *p_wall,
            0, 255, *p_pwm_r, *p_pwm_g, *p_pwm_b, *p_virtual_time);

        // OK, now get next command
        i_status = get_ascii_char( p_cmd, "[Font mode]");

        switch( (p_cmd)[0] )
        {
        // Next wall
        case 'n':
          (*p_wall)++;
          // Test if WALL is not higher than 4
          if(*p_wall > 4)
          {
            *p_wall = 4;
          }
          // And refresh screen
          show_preview( p_fb_data_virtual, *p_command_index, *p_frame, *p_wall,
              0, 255, *p_pwm_r, *p_pwm_g, *p_pwm_b, *p_virtual_time);
          break;

        // Back
        case 'b':
          (*p_wall)--;
          // Test if WALL is lower than 0 (8bit -> 255)
          if(*p_wall == 255)
          {
            *p_wall = 0;
          }
          // And refresh screen
          show_preview( p_fb_data_virtual, *p_command_index, *p_frame, *p_wall,
              0, 255, *p_pwm_r, *p_pwm_g, *p_pwm_b, *p_virtual_time);
          break;
        // Back
        case 'm':
          (*p_wall)--;
          // Test if WALL is lower than 0 (8bit -> 255)
          if(*p_wall == 255)
          {
            *p_wall = 0;
          }
          // And refresh screen
          show_preview( p_fb_data_virtual, *p_command_index, *p_frame, *p_wall,
              0, 255, *p_pwm_r, *p_pwm_g, *p_pwm_b, *p_virtual_time);
          break;

        // Done
        case 'd':
          i_status = done_command;
          break;

        // Help
        case 'h':
          show_help_font_mode();
          wait_for_user();
          break;

        // Rotations
        case 'r':
          //i_status = no_cancel_or_done_command;
          while(i_status == no_cancel_or_done_command)
          {   // Get data from keyboard
            i_status = get_ascii_string( p_cmd, "Rotation command");

            // Test for param_rotate_fb_forward
            if( (strcmp(p_cmd,"fwd")==0) ||
                (strcmp(p_cmd,"Fwd")==0) ||
                (strcmp(p_cmd,"FWD")==0) ||
                (strcmp(p_cmd,"f")  ==0) )
            {
              rotate_fb_forward(p_fb_data_virtual);
              // Add command
              p_commands[(*p_command_index)++] =
                  cmd_rotate_fb + param_rotate_fb_forward;
            }

            // Test for param_rotate_fb_backward
            if( (strcmp(p_cmd,"bwd")==0) ||
                (strcmp(p_cmd,"Bwd")==0) ||
                (strcmp(p_cmd,"BWD")==0) ||
                (strcmp(p_cmd,"b")  ==0) )
            {
              rotate_fb_backward(p_fb_data_virtual);
              // Add command
              p_commands[(*p_command_index)++] =
                  cmd_rotate_fb + param_rotate_fb_backward;
            }

            // Test for param_rotate_fb_left
            if( (strcmp(p_cmd,"left")==0) ||
                (strcmp(p_cmd,"Left")==0) ||
                (strcmp(p_cmd,"LEFT")==0) ||
                (strcmp(p_cmd,"l")   ==0) )
            {
              rotate_fb_left(p_fb_data_virtual);
              // Add command
              p_commands[(*p_command_index)++] =
                  cmd_rotate_fb + param_rotate_fb_left;
            }

            // Test for param_rotate_fb_right
            if( (strcmp(p_cmd,"right")==0) ||
                (strcmp(p_cmd,"Right")==0) ||
                (strcmp(p_cmd,"RIGHT")==0) ||
                (strcmp(p_cmd,"r")    ==0) )
            {
              rotate_fb_right(p_fb_data_virtual);
              // Add command
              p_commands[(*p_command_index)++] =
                  cmd_rotate_fb + param_rotate_fb_right;
            }

            // Test for param_rotate_fb_up
            if( (strcmp(p_cmd,"up")==0) ||
                (strcmp(p_cmd,"Up")==0) ||
                (strcmp(p_cmd,"UP")==0) ||
                (strcmp(p_cmd,"u") ==0) )
            {
              rotate_fb_up(p_fb_data_virtual);
              // Add command
              p_commands[(*p_command_index)++] =
                  cmd_rotate_fb + param_rotate_fb_up;
            }

            // Test for param_rotate_fb_down
            if( (strcmp(p_cmd,"down")==0) ||
                (strcmp(p_cmd,"Down")==0) ||
                (strcmp(p_cmd,"DOWN")==0) ||
                (strcmp(p_cmd,"j")   ==0) )
            {
              rotate_fb_down(p_fb_data_virtual);
              // Add command
              p_commands[(*p_command_index)++] =
                  cmd_rotate_fb + param_rotate_fb_down;
            }

            // Test for next
            if( (strcmp(p_cmd,"next")==0) ||
                (strcmp(p_cmd,"Next")==0) ||
                (strcmp(p_cmd,"NEXT")==0) ||
                (strcmp(p_cmd,"n")   ==0) )
            {
              (*p_wall)++;
              // Test if WALL is not higher than 4
              if(*p_wall > 4)
              {
                *p_wall = 4;
              }
            }

            // Test for back
            if( (strcmp(p_cmd,"back")==0) ||
                (strcmp(p_cmd,"Back")==0) ||
                (strcmp(p_cmd,"BACK")==0) ||
                (strcmp(p_cmd,"m")   ==0) )
            {
              (*p_wall)--;
              // Test if WALL is lower than 0 (8bit -> 255)
              if(*p_wall == 255)
              {
                *p_wall = 0;
              }
            }

            // Test for help
            if( (strcmp(p_cmd,"help")==0) ||
                (strcmp(p_cmd,"Help")==0) ||
                (strcmp(p_cmd,"HELP")==0) ||
                (strcmp(p_cmd,"h")   ==0) )
            {
              show_help_rotate_mode();
              wait_for_user();
            }


            // Test for done command
            if( (strcmp(p_cmd,"done")==0) ||
                (strcmp(p_cmd,"Done")==0) ||
                (strcmp(p_cmd,"DONE")==0) ||
                (strcmp(p_cmd,"d")   ==0) )
            {
              i_status = done_command;
            }

            jump_out_to_next_screen();
            // Refresh screen
            show_preview( p_fb_data_virtual, *p_command_index, *p_frame,
                *p_wall, 0, 255, *p_pwm_r, *p_pwm_g, *p_pwm_b,
                *p_virtual_time);
          }
          break;
        }
      }

      // Test for status - if 1 -> cancel command -> restore backup value
      if( i_status == cancel_command )
      {
        // Restore index
        (*p_command_index) = i_backup_command_index;
        // Clean last command
        p_commands[i_backup_command_index + 1] = 0;

        printf(""
"\n"
"+---------------------------------------------------------------------------|"
"\n"
"| Warning: character not saved as simple command. All commands are          |"
"\n"
"| discarded! However data in framebuffer stay.                              |"
"\n"
"| (Press Enter to proceed)                                                  |"
"\n"
"+---------------------------------------------------------------------------|"
"\n"
        );

        // And wait for user
        wait_for_user();
      }
      else
      {// Else there was "done" command
        // On the last position write 0
        (p_commands)[*p_command_index] = 0;
        // And load data to "real buffer"
        write_frame(p_fb_data_virtual,
                    p_anim_stream,
                    p_commands,
                    *p_frame,
                    1);

        *p_command_index = 0;    // After write reset
        p_commands[0] = 0;
        *p_wall=0;
        (*p_frame)++;    // Next frame -> increase
        // Recalculate time
        *p_virtual_time = *p_frame * (frame_period*(*p_slowdown_factor));

        // Clear framebuffer
        //clear_virtual_framebuffer( p_fb_data_virtual );       // DO NOT!!!
      }


    }
    // Else there was "cancel" - anyway clear p_cmd
    clear_loaded_command( p_cmd );

  }
}



/*---------------------------------------------------------------------------*/


void test_for_ROTATE(char *p_cmd,
                   uint8_t  *p_wall,
                   uint8_t  *p_pwm_r,
                   uint8_t  *p_pwm_g,
                   uint8_t  *p_pwm_b,
                   uint8_t  (*p_fb_data_virtual)[5][5][5],
                   uint16_t (*p_anim_stream)[max_size_anim_stream],
                   uint16_t (*p_commands),
                   uint16_t *p_command_index,
                   uint32_t *p_frame,
                   float    *p_virtual_time,
                   int      *p_slowdown_factor)
{
  if( (strcmp(p_cmd,"rotate")==0) ||
      (strcmp(p_cmd,"Rotate")==0) ||
      (strcmp(p_cmd,"ROTATE")==0) ||
      (strcmp(p_cmd,"t")     ==0) )
  {// Ask for command
    // Test for frame number. If 0 -> nothing to rotate
    if( *p_frame == 0 )
    {
      printf(""
"| Warning: Frame 0 -> nothing to rotate. Rotation aborted.                  |"
"\n"
"| (Press Enter to proceed)                                                  |"
"\n"
"+---------------------------------------------------------------------------|"
"\n" );
      wait_for_user();
      clear_loaded_command( p_cmd );
      return;
    }

    // Backup command index
    uint16_t i_backup_command_index = (*p_command_index);

    // Variable for status codes (error codes)
    uint8_t i_status = no_cancel_or_done_command;

    // Infinite loop - break if user type "cancel" or "done"
    while(i_status == no_cancel_or_done_command)
    {   // Get data from keyboard
      i_status = get_ascii_string( p_cmd, "Rotation command");

      // Test for param_rotate_fb_forward
      if( (strcmp(p_cmd,"fwd")==0) ||
          (strcmp(p_cmd,"Fwd")==0) ||
          (strcmp(p_cmd,"FWD")==0) ||
          (strcmp(p_cmd,"f")  ==0) )
      {
        rotate_fb_forward(p_fb_data_virtual);
        // Add command
        p_commands[(*p_command_index)++] =
            cmd_rotate_fb + param_rotate_fb_forward;
      }

      // Test for param_rotate_fb_backward
      if( (strcmp(p_cmd,"bwd")==0) ||
          (strcmp(p_cmd,"Bwd")==0) ||
          (strcmp(p_cmd,"BWD")==0) ||
          (strcmp(p_cmd,"b")  ==0) )
      {
        rotate_fb_backward(p_fb_data_virtual);
        // Add command
        p_commands[(*p_command_index)++] =
            cmd_rotate_fb + param_rotate_fb_backward;
      }

      // Test for param_rotate_fb_left
      if( (strcmp(p_cmd,"left")==0) ||
          (strcmp(p_cmd,"Left")==0) ||
          (strcmp(p_cmd,"LEFT")==0) ||
          (strcmp(p_cmd,"l")   ==0) )
      {
        rotate_fb_left(p_fb_data_virtual);
        // Add command
        p_commands[(*p_command_index)++] =
            cmd_rotate_fb + param_rotate_fb_left;
      }

      // Test for param_rotate_fb_right
      if( (strcmp(p_cmd,"right")==0) ||
          (strcmp(p_cmd,"Right")==0) ||
          (strcmp(p_cmd,"RIGHT")==0) ||
          (strcmp(p_cmd,"r")    ==0) )
      {
        rotate_fb_right(p_fb_data_virtual);
        // Add command
        p_commands[(*p_command_index)++] =
            cmd_rotate_fb + param_rotate_fb_right;
      }

      // Test for param_rotate_fb_up
      if( (strcmp(p_cmd,"up")==0) ||
          (strcmp(p_cmd,"Up")==0) ||
          (strcmp(p_cmd,"UP")==0) ||
          (strcmp(p_cmd,"u") ==0) )
      {
        rotate_fb_up(p_fb_data_virtual);
        // Add command
        p_commands[(*p_command_index)++] =
            cmd_rotate_fb + param_rotate_fb_up;
      }

      // Test for param_rotate_fb_down
      if( (strcmp(p_cmd,"down")==0) ||
          (strcmp(p_cmd,"Down")==0) ||
          (strcmp(p_cmd,"DOWN")==0) ||
          (strcmp(p_cmd,"j")   ==0) )
      {
        rotate_fb_down(p_fb_data_virtual);
        // Add command
        p_commands[(*p_command_index)++] =
            cmd_rotate_fb + param_rotate_fb_down;
      }

      // Test for next
      if( (strcmp(p_cmd,"next")==0) ||
          (strcmp(p_cmd,"Next")==0) ||
          (strcmp(p_cmd,"NEXT")==0) ||
          (strcmp(p_cmd,"n")   ==0) )
      {
        (*p_wall)++;
        // Test if WALL is not higher than 4
        if(*p_wall > 4)
        {
          *p_wall = 4;
        }
      }

      // Test for back
      if( (strcmp(p_cmd,"back")==0) ||
          (strcmp(p_cmd,"Back")==0) ||
          (strcmp(p_cmd,"BACK")==0) ||
          (strcmp(p_cmd,"m")   ==0) )
      {
        (*p_wall)--;
        // Test if WALL is lower than 0 (8bit -> 255)
        if(*p_wall == 255)
        {
          *p_wall = 0;
        }
      }

      // Test for help
      if( (strcmp(p_cmd,"help")==0) ||
          (strcmp(p_cmd,"Help")==0) ||
          (strcmp(p_cmd,"HELP")==0) ||
          (strcmp(p_cmd,"h")   ==0) )
      {
        show_help_rotate_mode();
        wait_for_user();
      }


      // Test for done command
      if( (strcmp(p_cmd,"done")==0) ||
          (strcmp(p_cmd,"Done")==0) ||
          (strcmp(p_cmd,"DONE")==0) ||
          (strcmp(p_cmd,"d")   ==0) )
      {
        i_status = done_command;
      }

      jump_out_to_next_screen();
      // Refresh screen
      show_preview( p_fb_data_virtual, *p_command_index, *p_frame, *p_wall,
          0, 255, *p_pwm_r, *p_pwm_g, *p_pwm_b, *p_virtual_time);
    }





    // Test for status - if 1 -> cancel command -> restore backup value
    if( i_status == cancel_command )
    {
      // Restore index
      (*p_command_index) = i_backup_command_index;
      // Clean last command
      p_commands[i_backup_command_index + 1] = 0;

      printf(""
"\n"
"+---------------------------------------------------------------------------|"
"\n"
"| Warning: Rotations not saved as simple command. All commands are          |"
"\n"
"| discarded! However data in framebuffer stay.                              |"
"\n"
"| (Press Enter to proceed)                                                  |"
"\n"
"+---------------------------------------------------------------------------|"
"\n"
      );

      // And wait for user
      wait_for_user();
    }
    else
    {// Else there was "done" command
      // On the last position write 0
      (p_commands)[*p_command_index] = 0;
      // And load data to "real buffer"
      write_frame(p_fb_data_virtual,
                  p_anim_stream,
                  p_commands,
                  *p_frame,
                  1);

      *p_command_index = 0;    // After write reset
      p_commands[0] = 0;
      *p_wall=0;
      (*p_frame)++;    // Next frame -> increase
      // Recalculate time
      *p_virtual_time = *p_frame * (frame_period*(*p_slowdown_factor));
    }


    // Always clear p_cmd
    clear_loaded_command( p_cmd );
  }
}




/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/


