/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Some functions are really simple, but they obstruct in main file
 */


#ifndef _simple_functions_library_
#define _simple_functions_library_

#include <inttypes.h>   // Data types
#include <stdio.h>      // Standard I/O

#include "font_5x5_PC.h"

#include "animation_codes.h"
#include "settings.h"

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
/**
 * \brief Show data in virtual framebuffer
 *
 * @param p_fb_data_virtual
 * @param i_frame_counter
 * @param i_wall
 * @param i_column
 * @param i_led_level
 * @param i_pwm_r
 * @param i_pwm_g
 * @param i_pwm_b
 * @param f_virtual_time
 */
void show_preview(uint8_t  (*p_fb_data_virtual)[5][5][5],
                  uint16_t i_command_index,
                  uint32_t i_frame_counter,
                  uint8_t  i_wall,
                  uint8_t  i_column,
                  uint8_t  i_led_level,
                  uint8_t  i_pwm_r,
                  uint8_t  i_pwm_g,
                  uint8_t  i_pwm_b,
                  float f_virtual_time);

/**
 * \brief Write actual frame (3D) to data stream
 *
 * @param p_fb_data_virtual Pointer do virtual framebuffer
 * @param p_anim_stream Pointer to serial data stream
 * @param i_frame Inform where continue
 * \param i_do_not_write_data Sometimes are data in framebuffer just product\n
 * of command -> do not write data from virtual framebuffer
 */
void write_frame(uint8_t  (*p_fb_data_virtual)[5][5][5],
                 uint16_t (*p_anim_stream)[max_size_anim_stream],
                 uint16_t (*p_commands),
                 uint32_t i_frame,
                 uint8_t  i_do_not_write_data);

/**
 * \brief Load input character to WALL0 in framebuffer
 * @param c_character Inpit character, that is searched in font database
 * @param p_fb_data_virtual Pointer to virtual framebuffer
 */
void load_character_to_framebuffer(char c_character,
                                   uint8_t  (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Clear input variable. Assume length "max_command_length"
 * @param p_c_cmd Input char to "clean" (NULL characters)
 */
void clear_loaded_command(char *p_cmd);

/**
 * \brief Clear (set 0) in whole framebuffer
 * @param p_fb_data_virtual Pointer to framebuffer
 */
void clear_virtual_framebuffer(uint8_t (*p_fb_data_vitual)[5][5][5]);

/**
 * \brief Set (set 1) in whole framebuffer
 * @param p_fb_data_virtual Pointer to framebuffer
 */
void set_virtual_framebuffer(uint8_t (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Clear real buffer (fill 0)
 * @param p_anim_stream
 */
void clear_real_framebuffer(uint16_t (*p_anim_stream)[max_size_anim_stream]);

/**
 * \brief Print a lots of newline characters, so actual screen go away
 */
void jump_out_to_next_screen(void);

/**
 * \brief Wait until user press "Enter" key
 */
void wait_for_user(void);


/**
 * \brief Show brief help for "Font mode"
 */
void show_help_font_mode(void);


/**
 * \brief Show brief help for "Rotate mode"
 */
void show_help_rotate_mode(void);

/**
 * \brief Rotate data in virtual framebuffer to forward
 */
void rotate_fb_forward(uint8_t (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Rotate data in virtual framebuffer to backward
 */
void rotate_fb_backward(uint8_t (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Rotate data in virtual framebuffer to left
 */
void rotate_fb_left(uint8_t (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Rotate data in virtual framebuffer to right
 */
void rotate_fb_right(uint8_t (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Rotate data in virtual framebuffer up
 */
void rotate_fb_up(uint8_t (*p_fb_data_virtual)[5][5][5]);

/**
 * \brief Rotate data in virtual framebuffer down
 */
void rotate_fb_down(uint8_t (*p_fb_data_virtual)[5][5][5]);

#endif
