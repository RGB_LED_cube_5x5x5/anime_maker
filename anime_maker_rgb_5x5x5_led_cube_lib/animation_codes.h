/**
 * \file
 *
 * \brief Animation codes used in animation stream
 *
 * Because for display 1 column is needed only 15 bits, there is one extra\n
 * bit, witch can signalizes that data are not directly for framebuffer, but\n
 * it is a special command witch, for example, can set PWM.\n
 * \n
 * Instruction format (if MSB is 1, then it is possible change some settings)\n
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n
 * | 1 | C | C | C | C | C | C | C | V | V | V | V | V | V | V | V |\n
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+\n
 *  C - command (0~127)\n
 *  V - value   (0~255)\n
 *
 * \author Martin Stejskal
 */


/**
 * \brief Command animation start
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8000.
 */
#define cmd_anim_start          (0x8000 + (0<<8))


/**
 * \brief Set PWM for red channel
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8100.
 */
#define cmd_set_pwm_r           (0x8000 + (1<<8))


/**
 * \brief Set PWM for green channel
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8200.
 */
#define cmd_set_pwm_g           (0x8000 + (2<<8))


/**
 * \brief Set PWM for blue channel
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8300.
 */
#define cmd_set_pwm_b           (0x8000 + (3<<8))


/*---------------------------------------------------------------------------*/
/**
 * \brief Parameters for 2D frame (activated wall)
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8400.
 */
#define cmd_param_2D_frame      (0x8000 + (4<<8))

/**
 * \brief Option for cmd_param_2D_frame - Void frame
 *
 * Used when in 2D frame are all LED off
 */
#define param_2D_frame_void     0

/**
 * \brief Option for cmd_param_2D_frame - Same 2D frame as previous wall
 *
 * It can be used for WALL1~WALL4 - NOT FOR WALL0
 */
#define param_2D_frame_same     1


/**
 * \brief Option for cmd_param_2D_frame - All LED on
 *
 * Used when in 2D frame are all LED on
 */
#define param_2D_frame_ones     2

/*---------------------------------------------------------------------------*/
/**
 * \brief Parameters for 3D frame (whole cube)
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8500.
 */
#define cmd_param_3D_frame      (0x8000 + (5<<8))

/**
 * \brief Option for cmd_param_3D_frame - Void frame
 *
 * Used when in 3D frame are all LED off
 */
#define param_3D_frame_void     0

/**
 * \brief Option for cmd_param_3D_frame - Same 3D frame as previous
 *
 * When this command is enabled, then content of framebuffer stay same
 */
#define param_3D_frame_same     1


/**
 * \brief Option for cmd_param_3D_frame - All LED on
 *
 * Used when in 3D frame are all LED on
 */
#define param_3D_frame_ones     2


/*---------------------------------------------------------------------------*/


/**
 * \brief Command draw character
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8600.
 */
#define cmd_draw_character      (0x8000 + (6<<8))


/*---------------------------------------------------------------------------*/


/**
 * \brief Command rotate data in framebuffer
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8700.
 */
#define cmd_rotate_fb           (0x8000 + (7<<8))

/**
 * \brief Direction for rotate command - forward
 *
 * Direction to user
 */
#define param_rotate_fb_forward         0

/**
 * \brief Direction for rotate command - backward
 *
 * Direction from user
 */
#define param_rotate_fb_backward        1

/**
 * \brief Direction for rotate command - left
 *
 * Direction to left from user view
 */
#define param_rotate_fb_left            2

/**
 * \brief Direction for rotate command - right
 *
 * Direction to right from user view
 */
#define param_rotate_fb_right           3

/**
 * \brief Direction for rotate command - up
 *
 * Direction to up from user view
 */
#define param_rotate_fb_up              4

/**
 * \brief Direction for rotate command - down
 *
 * Direction to down from user view
 */
#define param_rotate_fb_down            5

/*---------------------------------------------------------------------------*/

/**
 * \brief Random PWM value for all channels
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0x8800.
 */
#define cmd_rand_all_pwm        (0x8000 + (8<<8))

/*---------------------------------------------------------------------------*/


/**
 * \brief Command end of 3D frame
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0xFE00.
 */
#define cmd_end_of_3D_frame     (0x8000 + (126<<8))

/*---------------------------------------------------------------------------*/

/**
 * \brief Command end of animation
 *
 * Value 0x8000 means, that is command. Then is added command number and\n
 * shift to the left 8x (because first 8 bits is for command value).\n
 * So we get 0xFF00.
 */
#define cmd_end_of_anim         (0x8000 + (127<<8))
