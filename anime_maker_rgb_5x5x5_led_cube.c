/**
 * \file
 * \brief Anime maker v1.0
 *
 * Anime maker for RGB LED cube 5x5x5
 *
 * \author Martin Stejskal (martin.stej@gmail.com)
 */


#include <stdio.h>      // Standard I/O
#include <stdlib.h>     // Standard library
#include <string.h>     // Library for string operations

#include <inttypes.h>   // Data types

//#include <unistd.h> - if needed sleep function

#include "settings.h"   // Local settings for program

#include "animation_codes.h"    // Animation codes

#include "file_operations.h"
#include "ask_for.h"
#include "simple_functions.h"
#include "test_for.h"

#include "font_5x5_PC.h"        // Same font as for AVR (light modification)

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
void create_animation(char *p_animation_name, int i_slowdown_factor);

void write_frame(uint8_t  (*p_fb_data_virtual)[5][5][5],
                 uint16_t (*p_anim_stream)[max_size_anim_stream],
                 uint16_t (*p_commands),
                 uint32_t i_frame,
                 uint8_t  i_do_not_write_data);






/**
 * \brief Main program structure
 * @return 0 if all OK
 */
int main(void)
{

  char c_animation_name[name_len];      // Variable for animation name

  int  i_slowdown_factor;               // Slowdown factor

  // Ask for name of animation
  ask_for_anime_name( c_animation_name );

  // Create new animation file and fill headers
  create_files_and_fill_headers( c_animation_name );

  // Load slowdown factor
  i_slowdown_factor = ask_for_slow_down_factor();

  // Write start of animation
  write_start_of_animation( i_slowdown_factor, c_animation_name );

  // Create body of animation
  create_animation( c_animation_name, i_slowdown_factor );

  int i_repeat_animation  = ask_for_repeat_animation();

  // End of animation
  write_end_of_animation(i_repeat_animation, c_animation_name);

  // String for complete path (include filename and extension -> +2)
  char c_file_and_path[name_len + path_len +2];

  // Save string contains name of anime folder
  strcpy(c_file_and_path, generated_anime_folder);
  // Add slash and prefix anim
  strcat(c_file_and_path, "/anim_");
  // Add file name
  strcat(c_file_and_path, c_animation_name);


  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| Your animation files are:                                                 |"
"\n"
"| ./%s.c\n| ./%s.h\n"
"+---------------------------------------------------------------------------+"
"\n"
"                                   BYE!\n"
  ,c_file_and_path, c_file_and_path);


  /*
  int  dec = 32768;

  printf("\n HEX: %X | DEC: %d\n\n", dec, dec);
*/

  return(no_error);
}






void create_animation(char *p_animation_name, int i_slowdown_factor)
{
  // Info for user
  printf(""
"+---------------------------------------------------------------------------+"
"\n"
"| So, now i enable creative mode ;) There you can build your own animation. |"
"\n"
"| Ready? If yes, then enter                                                 |"
"\n"
"+---------------------------------------------------------------------------+"
"\n"
  );


  wait_for_user();


  // Create some variables
  uint32_t i_frame_counter = 0;             // Counts whole frames
  uint8_t i_wall = 0;                      // Number of active "wall" on LED cube
  uint8_t i_column = 0;                    // Active column on LED cube

  uint8_t i_pwm_r = 255;                   // PWM value for red LEDs
  uint8_t i_pwm_g = 255;                   // PWM value for green LEDs
  uint8_t i_pwm_b = 255;                   // PWM value for blue LEDs

  float f_virtual_time = 0;             // Virtual time of animation

  uint8_t i_led_level = 4;                // Highest LED

  // Virtual framebuffer (just for preview)
  // [wall][led level][column]
  uint8_t i_fb_data_virtual[5][5][5];

  // "Real" stream for LED cube
  uint16_t i_anim_stream[max_size_anim_stream];

  // Clear virtual framebuffer
  clear_virtual_framebuffer( &i_fb_data_virtual );

  // Clear real framebuffer
  clear_real_framebuffer( &i_anim_stream );

  // Array for commands (per whole frame)
  uint16_t i_commands[max_commands_per_frame];

  // Clear first in array
  i_commands[0] = 0;    // No command so far

  // Index for counting commands
  uint16_t i_command_index = 0;

  // Status variable
  int i_status = 0;

  // Sometimes data in framebuffer are just one command -> do not write them
  uint8_t i_do_not_write_data = 0;

  // Text array for command
  char c_cmd[max_command_length];


  int i_animation_done = 0;
  // Wait until animation is done
  while( i_animation_done == 0 )
  {
    // Clear screen
    jump_out_to_next_screen();
    // Show actual data
    show_preview( &i_fb_data_virtual,
                  i_command_index,
                  i_frame_counter,
                  i_wall,
                  i_column,
                  i_led_level,
                  i_pwm_r,
                  i_pwm_g,
                  i_pwm_b,
                  f_virtual_time);

    // Clear - for case that previous command will be shorter than 3 characters
    clear_loaded_command( c_cmd );

    // Load data from command line
    i_status = scanf("%s", c_cmd);
    if(i_status >1)
    {
      printf("| Wrong input??? \n\n");
    }
    // OK, let's test for possible commands


    // EXIT
    test_for_EXIT(c_cmd, &i_animation_done);

    // HELP
    test_for_HELP(c_cmd);

    // ZEROS
    test_for_ZEROS(c_cmd, &i_fb_data_virtual, i_wall);

    // ONES
    test_for_ONES(c_cmd, &i_fb_data_virtual, i_wall);

    // PWM red
    test_for_PWM_RED(c_cmd, &i_pwm_r, &i_commands, &i_command_index);

    // PWM green
    test_for_PWM_GREEN(c_cmd, &i_pwm_g, &i_commands, &i_command_index);

    // PWM blue
    test_for_PWM_BLUE(c_cmd, &i_pwm_b, &i_commands, &i_command_index);

    // FONT
    test_for_FONT(c_cmd, &i_wall, &i_pwm_r, &i_pwm_g,
        &i_pwm_b, &i_fb_data_virtual, &i_anim_stream, i_commands,
        &i_command_index, &i_frame_counter, &f_virtual_time,
        &i_slowdown_factor);

    // ROTATE
    test_for_ROTATE(c_cmd, &i_wall, &i_pwm_r, &i_pwm_g,
        &i_pwm_b, &i_fb_data_virtual, &i_anim_stream, i_commands,
        &i_command_index, &i_frame_counter, &f_virtual_time,
        &i_slowdown_factor);




    // LINE
    test_for_LINE(c_cmd, &i_fb_data_virtual, i_wall, i_led_level, i_column);

    // COLUMN
    test_for_COLUMN(c_cmd, &i_fb_data_virtual, i_wall, i_led_level, i_column);

    // RANDOM ALL PWM
    test_for_RANDOM_ALL_PWM(c_cmd, &i_commands, &i_command_index,
        &i_frame_counter, &i_fb_data_virtual, &i_anim_stream,
        &i_slowdown_factor, &i_wall, &f_virtual_time);



    // ARROW UP
    test_for_ARROW_UP(&c_cmd, &i_led_level);

    // ARROW DOWN
    test_for_ARROW_DOWN(&c_cmd, &i_led_level);

    // ARROW RIGHT
    test_for_ARROW_RIGHT(&c_cmd, &i_column);

    // ARROW LEFT
    test_for_ARROW_LEFT(&c_cmd, &i_column);

    // NEXT (2D frame - wall)
    test_for_NEXT(c_cmd, &i_column, &i_led_level, &i_wall, &i_fb_data_virtual,
        &i_anim_stream, i_commands,&i_command_index, &i_frame_counter,
        &f_virtual_time, &i_slowdown_factor, i_do_not_write_data);

    // BACK (2D frame - wall)
    test_for_BACK(c_cmd, &i_column, &i_led_level, &i_wall);


    // DATA - global - test if there is at least one combination
    test_for_DATA(&c_cmd, &i_column, &i_led_level, &i_wall, &i_fb_data_virtual,
        &i_anim_stream, i_commands,&i_command_index, &i_frame_counter,
        &f_virtual_time, &i_slowdown_factor, i_do_not_write_data);


  }



  // Exit


  // OK, now write data to file
  write_content_to_file( &i_anim_stream, i_frame_counter, p_animation_name);
}













