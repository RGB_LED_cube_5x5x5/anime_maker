# Makefile v1.1 - Martin Stejskal

# optimizer settings
optimize=O2

# fn - filename of "main" .c file
fn=anime_maker_rgb_5x5x5_led_cube

# dir_lib - user libraries folder
dir_lib=anime_maker_rgb_5x5x5_led_cube_lib

# Debug directory - for temporary compiled data.
dbg_dir=Debug

# Message for "all ok"
ok_msg=echo "" ; echo "<<|| All_OK ||>>"

all: compile link show_ok_msg

help:
	@echo "Parameters are:"
	@echo "		all - compile and link all sources"
	@echo "		clean - delete all files in \"$(dbg_dir)\" directory"
	@echo "Example: make all"
	@echo " This compile and link all sources -> make binary file"
	
show_ok_msg:
	@$(ok_msg)


compile:
	# Compile files in project root directory
	@gcc -Wall -$(optimize) -std=c99 -c *c -I ./$(dir_lib)/
	
	# Compile files in directory dir_lib
	@gcc -Wall -$(optimize) -std=c99 -c ./$(dir_lib)/*c

	# Move all *.o files to Debug directory 
	@mv *.o $(dbg_dir)/

link:
	# Linkage
	@gcc -Wall -o $(fn) $(dbg_dir)/*.o

run: compile link show_ok_msg run_program

run_program:
	@./$(fn)
	
clean: clean_remove_files show_ok_msg

clean_remove_files:
	@echo "Deleting files in Debug folder"
	@rm -rf Debug/*

